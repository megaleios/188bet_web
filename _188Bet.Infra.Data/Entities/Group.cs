﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    public class Group : ModelBase
    {
        public Group()
        {
            MemberId = new Dictionary<string, long>();
        }
        public string Name { get; set; }
        public string Photo { get; set; }
        public Dictionary<string,long> MemberId { get; set; }
        public string AdminId { get; set; }
        public override string CollectionName => nameof(Group);
    }
}
