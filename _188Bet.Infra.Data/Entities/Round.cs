﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Round : ModelBase
    {
        public string Name { get; set; }
        public long BeginDate { get; set; }
        public long EndDate { get; set; }
        public int Points { get; set; }
        public int Position { get; set; }
        public override string CollectionName => nameof(Round);
    }
}
