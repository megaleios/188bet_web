﻿using System;
using System.Collections.Generic;
using _188Bet.Infra.Data.Enum;

namespace _188Bet.Infra.Data.Entities
{
    public class Card
    {
        public string Title { get; set; }
        public string Question { get; set; }
        public TypeCard Type { get; set; }
        public bool Checked { get; set; }
        public List<Option> Options { get; set; }
    }
}
