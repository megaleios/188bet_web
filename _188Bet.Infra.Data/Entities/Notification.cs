﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    public class Notification : ModelBase
    {
        public string Text { get; set; }
        public string Photo { get; set; }
        public bool IsRead { get; set; }
        public override string CollectionName => nameof(Notification);
    }
}
