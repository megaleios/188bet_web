﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class State : ModelBase
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public bool Actived { get; set; }
        public string Code { get; set; }
        public override string CollectionName => nameof(State);
    }
}
