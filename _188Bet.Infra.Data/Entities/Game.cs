﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;
using _188Bet.Infra.Data.Enum;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Game : ModelBase
    {
        public Dictionary<string,string> TeamOne { get; set; }
        public Dictionary<string,string> TeamTwo { get; set; }
        public int GoalsTeamOne { get; set; }
        public int GoalsTeamTwo { get; set; }
        public long StartDate { get; set; }
        public long EndDate { get; set; }
        public int Bets { get; set; }
        public bool Actived { get; set; }
        public string Title { get; set; }
        public GameEnum Type { get; set; }
        public string RoundId { get; set; }
        public List<Card> Cards { get; set; }
        public override string CollectionName => nameof(Game);
    }
}
