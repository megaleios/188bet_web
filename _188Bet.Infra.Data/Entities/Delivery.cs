﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Delivery : ModelBase
    {
        public string Name { get; set; }
        public string CPF { get; set; }
        public string CEP { get; set; }
        public string Address { get; set; }
        public string Complement { get; set; }
        public string CityId { get; set; }
        public string StateId { get; set; }
        public string Telephone { get; set; }
        public int Status { get; set; }
        public string RewardId { get; set; }
        public string UserId { get; set; }
        public override string CollectionName => nameof(Delivery);
    }
}
