﻿using System;
using System.Collections.Generic;
using System.Text;
using _188Bet.Infra.Data.Enum;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Bet : ModelBase
    {
        public string UserId { get; set; }
        public Position UserChoice { get; set; }
        public TypeCard Type { get; set; }
        public string GameId { get; set; }
        public override string CollectionName => nameof(Bet);
    }
}
