﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    public class Title : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public override string CollectionName => nameof(Title);
    }
}
