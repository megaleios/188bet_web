﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class PointUser : ModelBase
    {
        public string UserId { get; set; }
        public  decimal Points { get; set; }
        public string BetId { get; set; }
        public string GameId { get; set; }
        public override string CollectionName => nameof(PointUser);
    }
}
