﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Reward : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Photo { get; set; }
        public List<string> Specifications { get; set; }
        public override string CollectionName => nameof(Reward);
    }
}
