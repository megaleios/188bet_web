﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Application.Core.ViewModels;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;
using _188Bet.Infra.Data.Enum;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class User : ModelBase
    {
        public User()
        {
            Team = new Dictionary<string, string>();
            State = new Dictionary<string, string>();
            Citie = new Dictionary<string, string>();
            BetId_GameId = new Dictionary<string, string>();
            Titles = new List<string>();
            Groups = new List<string>();
            GroupsRequest = new List<string>();
            Notification = new Dictionary<string, bool>();
            DeviceId = new List<string>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public long DateOfBirth { get; set; }
        public Dictionary<string, string> Team { get; set; }
        public Dictionary<string, string> State { get; set; }
        public Dictionary<string, string> Citie { get; set; }
        public Dictionary<string, bool> Notification { get; set; }
        public int InternationalGameWinCount { get; set; }
        public int NationalGameWinCount { get; set; }
        public string Password { get; set; }
        public string FacebookId { get; set; }
        public int Score { get; set; }
        public double Cashe { get; set; }
        public int Medal { get; set; }
        public UserLevelEnum Level { get; set; }
        //BetId/GameId
        public Dictionary<string,string> BetId_GameId { get; set; }

        //Titles
        public List<string> Titles { get; set; }

        //Groups Enter
        public List<string> Groups { get; set; }

        //Groups Request
        public List<string> GroupsRequest { get; set; }

        //Rewards Rescued
        public List<string> RewardsRescued { get; set; }

        public List<string> DeviceId { get; set; }
        public override string CollectionName => nameof(User);
    }
}
