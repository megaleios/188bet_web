﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;

namespace _188Bet.Infra.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Team : ModelBase
    {
        public string Name { get; set; }
        public override string CollectionName => nameof(Team);
    }
}
