﻿using System;
using _188Bet.Infra.Data.Enum;
using MongoDB.Bson;

namespace _188Bet.Infra.Data.Entities
{
    public class Option
    {
        public string Value { get; set; }
        public double Points { get; set; }
        public Position Position { get; set; }
        public string TeamId { get; set; }
        public bool Checked { get; set; }
    }
}
