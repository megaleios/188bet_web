﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _188Bet.Infra.Data.Enum
{
    public enum UserLevelEnum
    {
        New,
        Junior,
        Advanced,
        Legend
    }
}
