﻿using System;
namespace _188Bet.Infra.Data.Enum
{
    public enum TypeCard
    {
        TrueOrFalse,
        LeftAndRight,
        UpDown,
        ContainsTied
    }
}
