﻿using System;

public class ValidMessages
{
    //Date Exist Messages
    public const string UsernameExist = "Nome de usuario já esta em uso!";
    public const string EmailExist = "Email já esta em uso!";

    //Invalid Messages
    public const string UsernameInvalid = "Nome de usuario invalido!";
    public const string EmailInvalid = "Email invalido!";
    public const string PasswordInvalid = "Senha invalida, sua senha deve conter pelo menos 6 characters!";
    public const string ConfirmPasswordInvalid = "Senhas não conhecidem!";

    //Value Empty
    public const string StateIdEmpty = "Informe o Estado(UF)!";
}
