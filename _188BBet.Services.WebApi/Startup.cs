﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Infra.Core.MongoDb.Data.Database;
using UtilityFramework.Services.Core;
using UtilityFramework.Services.Core.Interface;
using _188Bet.Infra.Repository;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi
{
    public partial class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            BaseSettings.IS_DEV = env.IsDevelopment();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddAutoMapper();
            //SET Configuration 
            new BaseConfig(Configuration);

            services.AddSingleton(typeof(IMapper), typeof(Mapper));
            services.AddSingleton(typeof(ITeamService), typeof(TeamService));
            services.AddSingleton(typeof(IStateService), typeof(StateService));
            services.AddSingleton(typeof(ICitieService), typeof(CitieService));
            services.AddSingleton(typeof(IUserService), typeof(UserService));
            services.AddSingleton(typeof(IRoundService), typeof(RoundService));
            services.AddSingleton(typeof(IGameService), typeof(GameService));
            services.AddSingleton(typeof(IBetService), typeof(BetService));
            services.AddSingleton(typeof(IRewardService), typeof(RewardService));
            services.AddSingleton(typeof(IPointUserService), typeof(PointUserService));
            services.AddSingleton(typeof(ISenderMailService), typeof(SendService));
            services.AddSingleton(typeof(IGroupService), typeof(GroupService));
            services.AddSingleton(typeof(IDeliveryService), typeof(DeliveryService));
            services.AddSingleton(typeof(IFirebaseCloudMessageService), typeof(FirebaseCloudMessageService));
            services.AddSingleton(typeof(ITitleService), typeof(TitleService));
            services.AddSingleton(typeof(INotificationService), typeof(NotificationService));

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "188Bet-DEV", Version = "v1" });
                // documentação auth jwt
                c.OperationFilter<AddRequiredHeaderParameter>();
                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "XmlDocument.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            loggerFactory.AddFile("Logs/188Bet-{Date}.txt", LogLevel.Warning);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Content")),
                    RequestPath = new PathString("/Content")
                });
            }
            app.UseExceptionHandler(
                builder =>
                {
                    builder.Run(
                        async context =>
                        {

                            //if (context.Request.Path.Value.Contains("api/"))
                            //{
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.ContentType = "application/json";
                            var ex = context.Features.Get<IExceptionHandlerFeature>();
                            if (ex != null)
                            {
                                var err = JsonConvert.SerializeObject(new
                                {
                                    data = (string)null,
                                    erro = true,
                                    message = "Erro inesperado.",
                                    messageEx = $"{ex.Error.InnerException} {ex.Error.Message}",
                                    stacktrace = ex.Error.StackTrace,
                                    Code = 1
                                });
                                await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(err), 0, err.Length).ConfigureAwait(false);
                                //return;
                            }
                            //}
                            //app.UseExceptionHandler("/Home/Error");
                        });
                }
            );

            app.UseRequestResponseLogging();
            app.UseExceptionHandler("/Home/Error");
            ConfigureAuth(app);
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "188Bet");
            });
        }
    }
}
