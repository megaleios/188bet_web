﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FCM.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;
using UtilityFramework.Services.Core.Interface;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class UserController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IStateService _stateService;
        private readonly ICitieService _citieService;
        private readonly ITeamService _teamService;
        private readonly ISenderMailService _sendrMailService;
        private readonly IGroupService _groupService;
        private readonly IFirebaseCloudMessageService _firebaseCloudMessageService;
        private readonly IGameService _gameService;
        private readonly IBetService _betService;

        public UserController(IMapper mapper, IUserService userService, IStateService stateService, 
                              ICitieService citieService, ITeamService teamService, ISenderMailService sendrMailService, 
                              IGroupService groupService, IFirebaseCloudMessageService firebaseCloudMessageService, 
                              IGameService gameService, IBetService betService)
        {
            _mapper = mapper;
            _userService = userService;
            _stateService = stateService;
            _citieService = citieService;
            _teamService = teamService;
            _sendrMailService = sendrMailService;
            _groupService = groupService;
            _firebaseCloudMessageService = firebaseCloudMessageService;
            _gameService = gameService;
            _betService = betService;
        }


        /// <summary>
        /// Autenticação/RefreshToken
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "username": "dggarcia00",
        ///              "password":"123456",
        ///              "facebookId":null
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("Token")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Token([FromBody] LoginViewModel model)
        {
            try
            {
                model = model.TrimStringProperties();
                User entity;

                if (!string.IsNullOrEmpty(model.RefreshToken))
                {
                    var jwtToken = new JwtSecurityToken(model.RefreshToken.Replace("bearer ", "")
                        .Replace("Bearer ", "").Trim());

                    entity = _userService.FindById(jwtToken.Subject);

                    if (entity.LastUpdate != null && entity.LastUpdate > Utilities.ToTimeStamp(jwtToken.ValidFrom))
                        return BadRequest(Utilities.ReturnErro("Dados Invalidos"));

                    return Ok(Utilities.ReturnSuccess(
                        data: UtilityFramework.Application.Core.JwtMiddleware.TokenProviderMiddleware.GenerateToken(entity._id.ToString(), "/api/v1/User/Token", true)));

                }

                if (!string.IsNullOrEmpty(model.FacebookId))
                {
                    entity = _userService.FindOneBy(x => x.FacebookId == model.FacebookId);

                    if (entity == null)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                    return Ok(Utilities.ReturnSuccess(data: TokenProviderMiddleware.GenerateToken(entity._id.ToString(), "/api/v1/User/token")));

                }

                entity = _userService.FindOneBy(x => x.Username.Equals(model.Username));

                if (entity == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                model.Password = Utilities.GerarHashMd5(model.Password);

                entity =
                    _userService.FindOneBy(x => x.Username.Equals(entity.Username) && x.Password.Equals(model.Password));

                if (entity == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.PasswordInvalidLogin));

                return Ok(Utilities.ReturnSuccess(data: TokenProviderMiddleware.GenerateToken(entity._id.ToString(), "/api/v1/User/token")));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Cadastro de usuario
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///                 "firstName": "Diogo",
        ///                 "lastName": "Garcia",
        ///                 "username": "dggarcia00",
        ///                 "email": "dggarcia1@hotmail.com",
        ///                 "team": { "ABC" : "5a1ef7ca5ab25c3d8c905216" },
        ///                 "dateOfBirth": 880836669,
        ///                 "state": { "Acre": "5a1f07d784929747509a2bca" },
        ///                 "citie": { "Acrelândia": "5a1f099984929747509a2c4e" },
        ///                 "password": "123456",
        ///                 "confirmPassword": "123456"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("Register")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Register([FromBody]UserViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                var errors = ModelState.ValidModelState();

                if (errors != null)
                    return BadRequest(Utilities.ReturnErro(data: errors));

                if ((DateTime.Now.Year - Utilities.TimeStampToDateTime(model.DateOfBirth).Year) < 18)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.AgeInvalid));

                if (_userService.CheckBy(x => x.Email.Equals(model.Email.Trim())))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.EmailExist));

                if (_userService.CheckBy(x => x.Username.Equals(model.Username.Trim())))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UsernameExist.Trim()));

                var stateId = ObjectId.Parse(model.State.Id);

                if (!_stateService.CheckBy(x => x._id.Equals(stateId)))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.StateNotFound));

                var citieId = ObjectId.Parse(model.Citie.Id);

                if (!_citieService.CheckBy(x => x._id.Equals(citieId)))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.CitieNotFound));

                var teamId = ObjectId.Parse(model.Team.Id);

                if (!_teamService.CheckBy(x => x._id.Equals(teamId)))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.CitieNotFound));

                model.NewUser();
                model.Password = Utilities.GerarHashMd5(model.Password);
                model.Email = model.Email.ToLower();

                if (string.IsNullOrEmpty(model.Photo) && string.IsNullOrEmpty(model.FacebookId))
                    model.Photo = "profileDefault.jpg";

                if (!string.IsNullOrEmpty(model.FacebookId))
                    model.Photo = Utilities.GetPhotoFacebookGraph(model.FacebookId);

                _userService.Create(_mapper.Map<User>(model));
                return Token(!string.IsNullOrEmpty(model.FacebookId)
                    ? new LoginViewModel
                    {
                        FacebookId = model.FacebookId
                    }
                    : new LoginViewModel
                    {
                        Username = model.Username,
                        Password = Utilities.GerarHashMd5(model.Password)
                    });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Call Method in Login
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("SetDeviceId")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult SetDeviceId([FromBody]Device device)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (string.IsNullOrEmpty(device.DeviceId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.DeviceIdEmpty));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (user.DeviceId == null)
                    user.DeviceId = new List<string>();

                user.DeviceId.Add(device.DeviceId);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Call method in logout
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("DeleteDeviceId")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult RemoveDeviceId([FromBody] Device device)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (string.IsNullOrEmpty(device.DeviceId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.DeviceIdEmpty));

                if (user.DeviceId == null)
                    user.DeviceId = new List<string>();
                
                if (!user.DeviceId.Contains(device.DeviceId))
                    return Ok(Utilities.ReturnSuccess());

                user.DeviceId.Remove(device.DeviceId);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Partial Dashboard
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("Home")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Home()
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var model = _mapper.Map<MiniUserViewModel>(_userService.FindById(userId));
                model.SetPathInPhoto();

                model.Position = 1;
                model.Score = 1;

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Dados usuario
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("GetInfo")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult GetInfo()
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);
                var model = _mapper.Map<UserViewModel>(user);
                model.SetPathInPhoto();

                // State
                var stateId = user.State.FirstOrDefault().Value;
                if (stateId != null)
                    model.State = _mapper.Map<StateViewModel>(_stateService.FindById(stateId));

                // City
                var cityId = user.Citie.FirstOrDefault().Value;
                if (cityId != null)
                    model.Citie = _mapper.Map<CitieViewModel>(_citieService.FindById(cityId));

                // Team
                var teamId = user.Team.FirstOrDefault().Value;
                if (teamId != null){
                    var team = _mapper.Map<TeamViewModel>(_teamService.FindById(teamId));
                    team.SetPathInPhoto();
                    model.Team = team;
                }

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Atualiza perfil
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///             {
        ///                 "photo": "string",
        ///                 "firstName": "string", // required
        ///                 "lastName": "string", // required
        ///                 "email": "string", // required
        ///                 "team": { 
        ///                     "id" : "string" // required
        ///                 },
        ///                 "state": { 
        ///                     "id": "string" // required
        ///                 },
        ///                 "citie": { 
        ///                     "id": "string" // required
        ///                 }
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("Update")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Update([FromBody]UserUpdateViewModel model)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                //var errors = ModelState.ValidModelState(new[] { nameof(model.Password), nameof(model.ConfirmPassword) });

                //if (errors != null)
                //    return BadRequest(Utilities.ReturnErro(data: errors));

                //if ((DateTime.Now.Year - Utilities.TimeStampToDateTime(model.DateOfBirth).Year) < 18)
                    //return BadRequest(Utilities.ReturnErro(ValidMessages.AgeInvalid));

                var user = _userService.FindById(userId);

                if (_userService.CheckBy(x => x.Email.Equals(model.Email.Trim()) && !x._id.Equals(user._id)))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.EmailExist));

                //if (_userService.CheckBy(x => x.Username.Equals(model.Username.Trim()) && !x._id.Equals(user._id)))
                    //return BadRequest(Utilities.ReturnErro(ValidMessages.UsernameExist.Trim()));

                if (string.IsNullOrEmpty(model.Photo) && string.IsNullOrEmpty(user.FacebookId))
                    model.Photo = "profileDefault.jpg";

                user.FirstName = model.FirstName != null ? model.FirstName : user.FirstName;
                user.LastName = model.LastName != null ? model.LastName : user.LastName;
                user.Email = model.Email != null ? model.Email : user.Email;
                user.Photo = model.Photo != null ? model.Photo : user.Photo;
                // Team
                if (model.Team != null && model.Team.Id != null)
                {
                    var team = _teamService.FindById(model.Team.Id);
                    if(team == null)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.TeamNotFound));
                    
                    user.Team = new Dictionary<string, string> { { team.Name, team._id.ToString() } };
                }
                // State
                if (model.State != null && model.State.Id != null)
                {
                    var state = _stateService.FindById(model.State.Id);
                    if (state == null)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.StateNotFound));
                    
                    user.State = new Dictionary<string, string> { { state.Name, state._id.ToString() } };
                }
                // City
                if (model.Citie != null && model.Citie.Id != null)
                {
                    var city = _citieService.FindById(model.Citie.Id);
                    if (city == null)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.CitieNotFound));
                    
                    user.Citie = new Dictionary<string, string> { { model.State.Name, model.State.Id } };
                }

                //var entitUpdate = _mapper.Map<User>(model);
                //entitUpdate._id = user._id;
                //entitUpdate.Level = user.Level;
                //entitUpdate.Cashe = user.Cashe;
                //entitUpdate.Score = user.Score;
                //entitUpdate.Medal = user.Medal;
                //entitUpdate.DateOfBirth = user.DateOfBirth;
                //entitUpdate.Password = user.Password;
                //entitUpdate.Username = user.Username;
                //entitUpdate.LastUpdate = Utilities.ToTimeStamp(DateTime.Now);

                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("UpdatePassword")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult UpdatePassword([FromBody]PasswordViewModel model)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                var errors = ModelState.ValidModelState();

                if (errors != null)
                    return BadRequest(Utilities.ReturnErro(data: errors));

                if(!_userService.CheckBy(x=> x._id.Equals(new ObjectId(userId)) && x.Password.Equals(Utilities.GerarHashMd5(model.OldPassword))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.PasswordInvalidLogin));

                var user = _userService.FindById(userId);

                if(user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                user.Password = Utilities.GerarHashMd5(model.NewPassword);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess(ValidMessages.PasswordUpdatedSuccess));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Esqueci minha senha
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("ForgotPassword")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ForgotPassword([FromBody] UserViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Email))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.EmailEmpty));

                var user = _userService.FindOneBy(x => x.Email.Equals(model.Email));

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var dates = new Dictionary<string, string>();

                var newPassword = Utilities.RandomInt(6);

                dates.Add("{{user.Name}}", user.FirstName);
                dates.Add("{{user.password}}", newPassword);

                var body = _sendrMailService.GerateBody("forgotpassword", dates);

                _sendrMailService.SendMessageEmail("188Bet", user.Email, body, "Esqueci minha senha.", "188Bet");
                user.Password = Utilities.GerarHashMd5(newPassword);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Dados fakes
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("MyTitles")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult MyTitles()
        {
            try
            {
                var model = new List<TitleViewModel>
                {
                    new TitleViewModel
                    {
                        Photo = $"{BaseConfig.BaseUrl.Replace("upload","")}SystemImages\\folk.png",
                        Id = ObjectId.GenerateNewId().ToString(),
                        Name = "O Popular",
                        Acquired = true,
                        Description = "Convidar 10 amigos"
                    },
                    new TitleViewModel
                    {
                        Photo = $"{BaseConfig.BaseUrl.Replace("upload","")}SystemImages\\determined.png",
                        Id = ObjectId.GenerateNewId().ToString(),
                        Name = "O Determinado",
                        Acquired = true,
                        Description = "Preencher 24 rodadas seguidas"
                    }
                };
                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        /// DADOS FAKES
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("Notifications/{page}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Notifications([FromRoute]int page)
        {
            try
            {
                var notifications = new List<NotificationViewModel>
                {
                    new NotificationViewModel
                    {
                        Id = Guid.NewGuid().ToString(),
                        Photo = $"{BaseConfig.BaseUrl}imageNotFound.jpg",
                        IsRead = false,
                        Text = "Nova rodada disponivel, faça seus palpites agora!",
                        Date = Utilities.ToTimeStamp(DateTime.Now.AddHours(-1))
                    },
                    new NotificationViewModel
                    {
                        Id = Guid.NewGuid().ToString(),
                        Photo = $"{BaseConfig.BaseUrl}imageNotFound.jpg",
                        IsRead = false,
                        Text = "Nova rodada disponivel, faça seus palpites agora!",
                        Date = Utilities.ToTimeStamp(DateTime.Now.AddHours(-2))
                    },
                    new NotificationViewModel
                    {
                        Id = Guid.NewGuid().ToString(),
                        Photo = $"{BaseConfig.BaseUrl}imageNotFound.jpg",
                        IsRead = false,
                        Text = "Nova rodada disponivel, faça seus palpites agora!",
                        Date = Utilities.ToTimeStamp(DateTime.Now.AddHours(-3))
                    },
                    new NotificationViewModel
                    {
                        Id = Guid.NewGuid().ToString(),
                        Photo = $"{BaseConfig.BaseUrl}imageNotFound.jpg",
                        IsRead = false,
                        Text = "Nova rodada disponivel, faça seus palpites agora!",
                        Date = Utilities.ToTimeStamp(DateTime.Now.AddHours(-4))
                    }
                };

                return Ok(Utilities.ReturnSuccess(data: notifications));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        #region BetsMethods

        /// <summary>
        /// Dados Fakes (Meus chutes / Rodadas Anteriores)
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("Bets")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Bets()
        {
            try
            {
                // TODO Bets Fakes
                var modelList = new List<RoundViewModel>
                {
                    new RoundViewModel
                    {
                        Id = ObjectId.GenerateNewId().ToString(),
                        Position = 3456,
                        Name = "Rodada #23",
                        Points = 5,
                        BeginDate = 1508878269,
                        EndDate = 1509483069
                    },
                    new RoundViewModel
                    {
                        Id = ObjectId.GenerateNewId().ToString(),
                        Position = 3456,
                        Name = "Rodada #22",
                        Points = 22,
                        BeginDate = 1508878269,
                        EndDate = 1509483069
                    },
                    new RoundViewModel
                    {
                        Id = ObjectId.GenerateNewId().ToString(),
                        Position = 3456,
                        Name = "Rodada #21",
                        Points = 22,
                        BeginDate = 1508878269,
                        EndDate = 1509483069
                    }
                };

                return Ok(Utilities.ReturnSuccess(data: modelList));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Dados Fakes (Meus chutes / Rodadas Anteriores)
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("BetsByRound/{roundId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult BetsByRound([FromRoute] string roundId)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (string.IsNullOrEmpty(roundId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.RoundNotFound));

                // TODO Fake Round ID
                List<ObjectId> gamesIDs = new List<ObjectId>();
                user.BetId_GameId.Select(x => x.Value).ToList().ForEach(x => {
                    gamesIDs.Add(new ObjectId(x));
                });
                var games = _gameService.FindBy(x => gamesIDs.Contains(x._id)); //_gameService.FindBy(x => user.BetId_GameId.ContainsValue(x._id.ToString()) && x.RoundId == roundId);
                var models = _mapper.Map<List<GameDetailsViewModel>>(games);

                models.ForEach(model =>
                {
                    var game = games.Where(x => x._id.ToString() == model.Id).FirstOrDefault();

                    // Check if is Acquired
                    if (user.BetId_GameId.ContainsValue(model.Id))
                        model.Acquired = true;

                    // Bet
                    var betId = user.BetId_GameId.FirstOrDefault(x => x.Value == model.Id).Key;
                    if (betId != null){
                        var bet = _betService.FindById(betId);
                        if (bet != null) {
                            model.Cards.ForEach(card => {
                                if (card.Type == bet.Type)
                                    card.Checked = true;

                                card.Options.ForEach(option => {
                                    if (card.Type == bet.Type && option.Position == bet.UserChoice)
                                        option.Checked = true;

                                    // teams
                                    var cardEntity = game.Cards.FirstOrDefault(x => x.Type == card.Type);
                                    if (cardEntity != null)
                                    {
                                        var optionEntity = cardEntity.Options.FirstOrDefault(x => x.Position == option.Position);
                                        if (optionEntity != null && optionEntity.TeamId != null)
                                        {
                                            var t = _mapper.Map<TeamViewModel>(_teamService.FindById(optionEntity.TeamId));
                                            t.SetPathInPhoto();
                                            option.Team = t;
                                        }
                                    }
                                });
                            });
                        }
                    }
                });

                return Ok(Utilities.ReturnSuccess(data: models));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        #endregion
        
        #region GroupMethods
        /// <summary>
        /// Criar Grupo
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///                 "name": "Megaleios",
        ///                 "photo": "171129202402.png
        ///                 "members": [
        ///                 {
        ///                     "id": "5a1f785b73ea5946dcf5f937",
        ///                     "isAdmin": false
        ///                 },
        ///                 {
        ///                     "id": "5a1f78fd73ea5946dcf5f939",
        ///                     "isAdmin": false
        ///                 },
        ///                 {
        ///                     "id": "5a229cb67c1b260588f95187",
        ///                     "isAdmin": false
        ///                 }
        ///                 ]
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("CreateGroup")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult CreateGroup([FromBody] GroupViewModel model)
        {
            try
            {
                var userId = Request.GetUserId();

                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var errors = ModelState.ValidModelState();

                if (errors != null)
                    return BadRequest(Utilities.ReturnErro(data: errors));

                if (_groupService.CheckBy(x=> x.Name.Equals(model.Name)))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNameExist));

                if (model.Members.Count == 0)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.QuantityMembersInvalid));

                if (model.Members.Count(x => x.IsAdmin) > 1)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UniqueAdminMember));

                //Search Users For Validation
                model.Members.ForEach(member =>
                {
                    if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(member.Id))))
                    {
                        BadRequest(!string.IsNullOrEmpty(member.Username)
                            ? Utilities.ReturnErro($"{member.Username}  {ValidMessages.UserNotFoundGroup}")
                            : Utilities.ReturnErro($"{ValidMessages.UserNotFound}"));
                    }
                });

                var admin = _mapper.Map<MemberGroupViewModel>(user);
                admin.IsAdmin = true;
                model.Members.Add(admin);

                if (string.IsNullOrEmpty(model.Photo))
                    model.Photo = "imageNotFound.jpg";

                var groupId = _groupService.Create(_mapper.Map<Group>(model));
                var group = _groupService.FindById(groupId);
                //Request Group User
                model.Members.ForEach(member =>
                {
                    if (!member.Id.Equals(userId))
                    {
                        var memberUser = _userService.FindById(member.Id);
                        memberUser?.GroupsRequest.Add(groupId);
                        _userService.Update(memberUser);

                        //TODO SET GENERIC SERVER KEY
                        _firebaseCloudMessageService.SendPushNotification("188Bet",
                            $"{user.Username} te convidou para {group.Name}", memberUser.DeviceId, Priority.High,
                            DateTime.Now,
                            "AAAAVGokZUQ:APA91bGqiKo5026KEPzG0w6nUwypU4zeqK8Ueu9UYliJT0i2D4WwFD20IwvNFla0j3d7gbF3aHCb_7JTWQizzalAjvE6u8yfcNE2MluOekqD2R-YTTNSkuXev6vcynyYhcXbjTrtqEXD");

                    }
                });
                
                user.Groups.Add(groupId);
                group.MemberId.Add(user._id.ToString(),Utilities.ToTimeStamp(DateTime.Now));
                group.AdminId = userId;

                _groupService.Update(group);
                _userService.Update(user);
                
                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Aceitar convite
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///             {
        ///                 "id":"string" // required group id
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("AcceptRequestGroup")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult AcceptRequestGroup([FromBody] GroupViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                user.GroupsRequest.Remove(model.Id);
                user.Groups.Add(model.Id);

                group.MemberId.Add(userId,Utilities.ToTimeStamp(DateTime.Now));

                _groupService.Update(group);
                _userService.Update(user);
                
                return Ok(Utilities.ReturnSuccess($"{ValidMessages.EnterInGroup} {group.Name}"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Recusar convite de grupo
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("DennyRequestGroup")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult DennyRequestGroup([FromBody] GroupViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                user.GroupsRequest.Remove(model.Id);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess($"{ValidMessages.DennyRequestGroup}"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Pesquisar usuario pelo nick
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("SeachByUsername")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult SeachByUsername(string username)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var target = _userService.GetByUsername(username);

                if (target == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var model = _mapper.Map<MiniUserViewModel>(target);
                model.SetPathInPhoto();
                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Solicitações de grupos
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("MyGroupRequest")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult MyGroupRequest()
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var modelList = new List<GroupViewModel>();

                user.GroupsRequest.ForEach(groupId =>
                {
                    var group = _groupService.FindById(groupId);

                    if (group != null)
                    {
                        var model = new GroupViewModel
                        {
                            Id = groupId,
                            Photo = $"{BaseConfig.BaseUrl}{group.Photo}",
                            Name = group.Name
                        };

                        var users = _userService.FindBy(x => x.Groups.Count > 0 && x.Groups.Contains(groupId)).ToList();

                        users.ForEach(x =>
                        {
                            // TODO member position
                            var position = 12;
                            model.Members.Add(new MemberGroupViewModel
                            {
                                Id = x._id.ToString(),
                                Username = x.Username,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                Level = x.Level,
                                Photo = string.IsNullOrEmpty(x.FacebookId) ? $"{BaseConfig.BaseUrl}{x.Photo}" : Utilities.GetPhotoFacebookGraph(x.FacebookId),
                                Position = position,
                                IsAdmin = x._id.ToString().Equals(group.AdminId)
                            });
                            position++;
                        });
                        modelList.Add(model);
                    }
                });

                return Ok(Utilities.ReturnSuccess(data: modelList));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Grupos participantes
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("MyGroupList")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult MyGroupList()
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var modelList = new List<GroupViewModel>();

                user.Groups.ForEach(groupId =>
                {
                    var group = _groupService.FindById(groupId);

                    if (group != null)
                    {
                        var model = new GroupViewModel
                        {
                            Id = groupId,
                            Photo = $"{BaseConfig.BaseUrl}{group.Photo}",
                            Name = group.Name
                        };

                        // Members
                        var users = _mapper.Map<List<UserViewModel>>(_userService.FindBy(x => x.Groups.Count > 0 && x.Groups.Contains(groupId)).ToList());
                        users.ForEach(x =>
                        {
                            x.SetPathInPhoto();
                            // TODO member position
                            var position = 12;
                            model.Members.Add(new MemberGroupViewModel
                            {
                                Id = x.Id,
                                Username = x.Username,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                Photo = x.Photo,
                                Level = x.Level,
                                Position = position,
                                IsAdmin = x.Id.Equals(group.AdminId)
                            });
                            position++;
                        });

                        // Invited Users
                        var invitedUsers = _mapper.Map<List<InvitedUserGroupViewModel>>(_userService.FindBy(x => x.GroupsRequest.Count() > 0 && x.GroupsRequest.Contains(group._id.ToString())));
                        invitedUsers.ForEach(modelInvitedUser => {
                            var invitedUser = _mapper.Map<UserViewModel>(_userService.FindById(modelInvitedUser.Id));
                            invitedUser.SetPathInPhoto();
                            modelInvitedUser.Photo = invitedUser.Photo;
                        });
                        model.InvitedUsers = invitedUsers;

                        modelList.Add(model);
                    }
                });
                return Ok(Utilities.ReturnSuccess(data: modelList));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }



        #endregion

    }
}
