﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Org.BouncyCastle.Asn1.Ocsp;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Repository.Interface;
using _188Bet.Infra.Data.Enum;

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class BetController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;
        private readonly IBetService _betService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="gameService"></param>
        /// <param name="userService"></param>
        /// <param name="teamService"></param>
        /// <param name="betService"></param>
        public BetController(IMapper mapper, IGameService gameService, IUserService userService, ITeamService teamService, IBetService betService)
        {
            _mapper = mapper;
            _gameService = gameService;
            _userService = userService;
            _teamService = teamService;
            _betService = betService;
        }


        /// <summary>
        /// Bets fake
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("ByRound/{roundId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ByRound([FromRoute] string roundId)
        {
            try
            {

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                // TODO Bets by round fake
                var gameEntity = _gameService.FindAll().FirstOrDefault();
                var game = _mapper.Map<MiniGameViewModel>(gameEntity);

                // Teams
                game.TeamOne = _mapper.Map<TeamViewModel>(_teamService.FindById(gameEntity.TeamOne.FirstOrDefault().Value));
                game.TeamTwo = _mapper.Map<TeamViewModel>(_teamService.FindById(gameEntity.TeamTwo.FirstOrDefault().Value));

                var bets = new List<BetViewModel>
                {
                    new BetViewModel
                    {
                        UserChoice = Position.Down,
                        Type = TypeCard.ContainsTied,
                        Game = game
                    },
                    new BetViewModel
                    {
                        UserChoice = Position.Up,
                        Type = TypeCard.UpDown,
                        Game = game
                    },
                    new BetViewModel
                    {
                        UserChoice = Position.Left,
                        Type = TypeCard.LeftAndRight,
                        Game = game
                    }
                };

                return Ok(Utilities.ReturnSuccess(data: bets));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Bets Details
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("Detail/{betId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Detail([FromRoute] string betId)
        {
            try
            {

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var bet = _betService.FindById(betId);

                if (bet != null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.BetNotFound));

                var gameEntity = _gameService.FindById(bet.GameId);
                var game = _mapper.Map<MiniGameViewModel>(gameEntity);

                // Teams
                game.TeamOne = _mapper.Map<TeamViewModel>(_teamService.FindById(gameEntity.TeamOne.FirstOrDefault().Value));
                game.TeamTwo = _mapper.Map<TeamViewModel>(_teamService.FindById(gameEntity.TeamTwo.FirstOrDefault().Value));

                var model = _mapper.Map<BetViewModel>(bet);
                model.Game = game;

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }


        /// <summary>
        /// Bet register
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///          {
        ///              "userChoice": 0, // required user choice, TypeCard enum { 0 = up, 1 = down, 2 = left, 3 = right }
        ///              "type": 0, // required bet type, TypeCard enum { 0 = TrueOrFalse, 1 = LeftAndRight, 2 = UpDown, 3 = ContainsTied }
        ///              "gameId": "string" // required game id
        ///          }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody] NewBetViewModel model)
        {
            try
            {

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                var errors = ModelState.ValidModelState();

                if (errors != null)
                    return BadRequest(Utilities.ReturnErro(data: errors));

                var user = _userService.FindById(userId);

                if (user.BetId_GameId.ContainsValue(model.GameId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.BetExist));

                var gameId = ObjectId.Parse(model.GameId);

                if (!_gameService.CheckBy(x => x._id.Equals(gameId) && x.Actived))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GameNotFound));

                model.UserId = user._id.ToString();
                var bet = _betService.CreateReturn(_mapper.Map<Bet>(model));
                user.BetId_GameId.Add(bet._id.ToString(), model.GameId);
                _userService.Update(user);

                var game = _gameService.FindById(model.GameId);
                game.Bets++;
                _gameService.Update(game);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
