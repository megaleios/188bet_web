﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FCM.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    public class GroupController : Controller
    {

        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IFirebaseCloudMessageService _firebaseCloudMessageService;

        public GroupController(IMapper mapper, IUserService userService, IGroupService groupService, IFirebaseCloudMessageService firebaseCloudMessageService)
        {
            _mapper = mapper;
            _userService = userService;
            _groupService = groupService;
            _firebaseCloudMessageService = firebaseCloudMessageService;
        }


        /// <summary>
        ///  APENAS POSIÇÃO FAKE
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Home(string id)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (!_groupService.CheckBy(x => x._id.Equals(new ObjectId(id))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!user.Groups.Contains(id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NotParticipatesGroup));

                var group = _groupService.FindById(id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                // Group
                var model = _mapper.Map<GroupViewModel>(group);

                // Members
                foreach (var memberId in group.MemberId)
                {
                    if (!string.IsNullOrEmpty(memberId.Key))
                    {
                        var member = _userService.FindById(memberId.Key);

                        if (member != null)
                        {
                            var modelMember = _mapper.Map<MemberGroupViewModel>(member);
                            modelMember.Photo = !string.IsNullOrEmpty(member.FacebookId)
                                ? Utilities.GetPhotoFacebookGraph(member.FacebookId)
                                : $"{BaseConfig.BaseUrl}{member.Photo}";
                            modelMember.IsAdmin = group.AdminId.Equals(modelMember.Id);

                            //TODO DATE FAKE TEMP
                            modelMember.Position = 1;

                            model.Members.Add(modelMember);
                        }
                    }
                }

                // Invited Users
                var invitedUsers = _mapper.Map<List<InvitedUserGroupViewModel>>(_userService.FindBy(x => x.GroupsRequest.Count() > 0 && x.GroupsRequest.Contains(group._id.ToString())));
                invitedUsers.ForEach(modelInvitedUser => {
                    var invitedUser = _mapper.Map<UserViewModel>(_userService.FindById(modelInvitedUser.Id));
                    invitedUser.SetPathInPhoto();
                    modelInvitedUser.Photo = invitedUser.Photo;
                });
                model.InvitedUsers = invitedUsers;

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Sair do grupo
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///             {
        ///                 "id":"string" // required group id
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("Exit")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Exit([FromBody] GroupViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (!_groupService.CheckBy(x => x._id.Equals(new ObjectId(model.Id))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!user.Groups.Contains(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NotParticipatesGroup));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (group.AdminId.Equals(userId))
                {
                    var newAdmin = group.MemberId.Where(x => !x.Key.Equals(userId)).OrderByDescending(x => x.Value).FirstOrDefault();

                    //DELETE GROUP
                    if (string.IsNullOrEmpty(newAdmin.Key))
                    {
                        //return BadRequest(Utilities.ReturnErro(ValidMessages.NoSearchNewAdmin));
                       
                        //REMOVE REQUESTS MEMBERS
                        var membersRequest = _userService.FindBy(x => x.GroupsRequest.Contains(model.Id)).ToList();

                        if (membersRequest.Count > 0)
                        {
                            membersRequest.ForEach(member =>
                            {
                                member.GroupsRequest.Remove(model.Id);
                                _userService.Update(member);
                            });
                        }

                        user.Groups.Remove(model.Id);
                        _groupService.DeleteOne(model.Id);

                        return Ok(Utilities.ReturnSuccess($"{ValidMessages.ExitGroupSuccess} {group.Name}"));
                    }
                        

                    var newUserAdmin = _userService.FindById(newAdmin.Key);

                    if (newUserAdmin != null)
                    {
                        group.AdminId = newUserAdmin._id.ToString();

                        group.MemberId.Remove(userId);
                        _groupService.Update(group);
                    }
                    else
                    {
                        _groupService.DeleteOne(group._id.ToString());
                    }
                }

                user.Groups.Remove(model.Id);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess($"{ValidMessages.ExitGroupSuccess} {group.Name}"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Remover Membro
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///             {
        ///                 "members": [{
        ///                     "id": "string" // required user id
        ///                 }],
        ///                 "id": "string" // required group id
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("RemoveMember")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult RemoveMember([FromBody]GroupViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                if (model.Members.Count == 0)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.SelectOneItemForDelete));

                if (model.Members.Count > 1)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.DeleteOneItem));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (!_groupService.CheckBy(x => x._id.Equals(new ObjectId(model.Id))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!user.Groups.Contains(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NotParticipatesGroup));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!group.AdminId.Equals(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.YouNoAdminGroup));

                var memberId = model.Members.FirstOrDefault().Id;
                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(memberId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TargetUserNotFound));

                var target = _userService.FindById(model.Members.FirstOrDefault().Id);

                if (target == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TargetUserNotFound));

                if (!target.Groups.Contains(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TargetUserNotParticipateGroup));

                target.Groups.Remove(model.Id);
                group.MemberId.Remove(target._id.ToString());

                _groupService.UpdateOne(group);
                _userService.UpdateOne(target);


                model = model.TrimStringProperties();
                var errorMessage = model.ValidModel(new[] { nameof(model) });

                if (!string.IsNullOrEmpty(errorMessage))
                    return BadRequest(Utilities.ReturnErro(errorMessage));

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Convidar usuario
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///             {
        ///                 "members": [{
        ///                     "id": "string" // required user id
        ///                 }],
        ///                 "id": "string" // required group id
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("InviteNewMember")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult InviteNewMember([FromBody] GroupViewModel model)
        {
            try
            {

                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                if (model.Members.Count == 0)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.SelectOneItemForAdd));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (!_groupService.CheckBy(x => x._id.Equals(new ObjectId(model.Id))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!user.Groups.Contains(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NotParticipatesGroup));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!group.AdminId.Equals(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.YouNoAdminGroup));

                var memberId = model.Members.FirstOrDefault().Id;
                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(memberId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TargetUserNotFound));

                model.Members.ForEach(member =>
                {
                    var userTarget = _userService.FindById(member.Id);

                    if (userTarget != null)
                    {
                        if (!userTarget.Groups.Contains(model.Id) && !userTarget.GroupsRequest.Contains(group._id.ToString()))
                        {
                            //TODO SET SERVER KEY GENERIC
                            userTarget.GroupsRequest.Add(group._id.ToString());
                            _firebaseCloudMessageService.SendPushNotification("188Bet",
                                $"{user.Username} te convidou para {group.Name}", userTarget.DeviceId, Priority.High,
                                DateTime.Now,
                                "AAAAVGokZUQ:APA91bGqiKo5026KEPzG0w6nUwypU4zeqK8Ueu9UYliJT0i2D4WwFD20IwvNFla0j3d7gbF3aHCb_7JTWQizzalAjvE6u8yfcNE2MluOekqD2R-YTTNSkuXev6vcynyYhcXbjTrtqEXD");
                            _userService.Update(userTarget);
                        }
                    }
                });

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Atualizar grupo
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("Update")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Update([FromBody] GroupViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (!_groupService.CheckBy(x => x._id.Equals(new ObjectId(model.Id))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!user.Groups.Contains(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NotParticipatesGroup));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!group.AdminId.Equals(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.YouNoAdminGroup));

                group.Photo = model.Photo;
                group.Name = model.Name;

                _groupService.Update(group);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        /// Deletar grupo
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("Delete")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Delete([FromBody]GroupViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupIdEmpty));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (!_groupService.CheckBy(x => x._id.Equals(new ObjectId(model.Id))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));

                if (!user.Groups.Contains(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NotParticipatesGroup));

                var group = _groupService.FindById(model.Id);

                if (group == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GroupNotFound));


                if (!group.AdminId.Equals(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.YouNoAdminGroup));

                //REMOVE MEMBERS
                group.MemberId.ToList().ForEach(memberId =>
                {
                    if (!string.IsNullOrEmpty(memberId.Key))
                    {
                        var memberProfile = _userService.FindById(memberId.Key);

                        if (memberProfile != null)
                        {
                            memberProfile.Groups.Remove(model.Id);
                            _userService.Update(memberProfile);
                        }
                    }
                });
             
                //REMOVE REQUESTS MEMBERS
                var membersRequest = _userService.FindBy(x => x.GroupsRequest.Contains(model.Id)).ToList();

                if(membersRequest.Count > 0)
                {
                    membersRequest.ForEach(member =>
                    {
                        member.GroupsRequest.Remove(model.Id);
                        _userService.Update(member);
                    });
                }

                user.Groups.Remove(model.Id);
                _groupService.DeleteOne(model.Id);
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess($"{ValidMessages.GroupDeleted} {group.Name}"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
