﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class RankingController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IStateService _stateService;
        private readonly ICitieService _citieService;
        private readonly ITeamService _teamService;
        private readonly IPointUserService _pointUserService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="userService"></param>
        /// <param name="stateService"></param>
        /// <param name="citieService"></param>
        /// <param name="teamService"></param>
        /// <param name="pointUserService"></param>
        public RankingController(IMapper mapper, IUserService userService, IStateService stateService, ICitieService citieService, ITeamService teamService, IPointUserService pointUserService)
        {
            _mapper = mapper;
            _userService = userService;
            _stateService = stateService;
            _citieService = citieService;
            _teamService = teamService;
            _pointUserService = pointUserService;
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Week/{page}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Week([FromRoute]int page)
        {
            try
            {
                //var userId = Request.GetUserId();

                //if (string.IsNullOrEmpty(userId))
                //    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                //if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                //    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var rangeStartDate = Utilities.ToTimeStamp(new DateTime(DateTime.UtcNow.Year, 11, DateTime.UtcNow.Date.AddDays(-7).Day,0,0,0));
                //temp 
                rangeStartDate = 1512012190;
                //var pointUsers = _pointUserService.FindBy(x => x.Created >= rangeStartDate && x._id != new ObjectId(userId),page).ToList();
                var pointUsers = _pointUserService.FindBy(x => x.Created >= rangeStartDate,page).ToList();
                var userRanking = new List<MiniUserViewModel>();
                var position = 1;


                if (page == 1)
                    position += 30;

                if (page > 1)
                    position = (page * 30) + 1;
                
                pointUsers.ForEach(x =>
                {
                    if (!userRanking.Exists(user => user.Id.Equals(x.UserId)))
                    {
                        var user = _userService.FindById(x.UserId);
                        var model = _mapper.Map<MiniUserViewModel>(user);

                        if (!string.IsNullOrEmpty(user.FacebookId))
                        {
                            model.Photo = Utilities.GetPhotoFacebookGraph(user.FacebookId);
                        }

                        else
                            model.Photo = $"{BaseConfig.BaseUrl}{user.Photo}";

                        model.Score = pointUsers.FindAll(u => u.UserId.Equals(x.UserId)).Sum(up => up.Points);
                        
                        userRanking.Add(model);
                    }
                });

               var modeList =  userRanking.OrderByDescending(x => x.Score).ToList();

                modeList.ForEach(user =>
                {
                    user.Position = position;
                    position++;
                });
                
                return Ok(Utilities.ReturnSuccess(data: userRanking.OrderBy(x=> x.Position)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("MyPositionWeek")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult MyPositionWeek()
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var rangeStartDate = Utilities.ToTimeStamp(new DateTime(DateTime.UtcNow.Year, 11, DateTime.UtcNow.Date.AddDays(-7).Day, 0, 0, 0));
                var pointUsers = _pointUserService.FindBy(x => x.Created >= rangeStartDate && x._id != new ObjectId(userId)).ToList();
                var userRanking = new List<MiniUserViewModel>();
                var position = 1;

                pointUsers.ForEach(x =>
                {
                    if (!userRanking.Exists(user => user.Id.Equals(x.UserId)))
                    {
                        var user = _userService.FindById(x.UserId);
                        var model = _mapper.Map<MiniUserViewModel>(user);

                        if (!string.IsNullOrEmpty(user.FacebookId))
                        {
                            model.Photo = Utilities.GetPhotoFacebookGraph(user.FacebookId);
                        }

                        else
                            model.Photo = $"{BaseConfig.BaseUrl}{user.Photo}";

                        model.Score = pointUsers.FindAll(u => u.UserId.Equals(x.UserId)).Sum(up => up.Points);

                        userRanking.Add(model);
                    }
                });

                var modeList = userRanking.OrderByDescending(x => x.Score).ToList();

                modeList.ForEach(user =>
                {
                    user.Position = position;
                    position++;
                });
                
                return Ok(Utilities.ReturnSuccess(data: modeList.FirstOrDefault(x=> x.Id.Equals(userId))));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Month/{page}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Month([FromRoute]int page)
        {
            try
            {
                //var userId = Request.GetUserId();

                //if (string.IsNullOrEmpty(userId))
                //    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                //if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                //    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var rangeStartDate = Utilities.ToTimeStamp(new DateTime(DateTime.UtcNow.Year,11, 1, 0, 0, 0));
                //temp 
                rangeStartDate = 1512012190;
                //var pointUsers = _pointUserService.FindBy(x => x.Created >= rangeStartDate && x._id != new ObjectId(userId), page).ToList();
                var pointUsers = _pointUserService.FindBy(x => x.Created >= rangeStartDate, page).ToList();
                var userRanking = new List<MiniUserViewModel>();
                var position = 1;

                if (page == 1)
                    position += 30;

                if (page > 1)
                    position = (page * 30) + 1;

                pointUsers.ForEach(x =>
                {
                    if (!userRanking.Exists(user => user.Id.Equals(x.UserId)))
                    {
                        var user = _userService.FindById(x.UserId);
                        var model = _mapper.Map<MiniUserViewModel>(user);
                        model.Score = pointUsers.FindAll(u => u.UserId.Equals(x.UserId)).Sum(up => up.Points);

                        if (!string.IsNullOrEmpty(user.FacebookId))
                        {
                            model.Photo = Utilities.GetPhotoFacebookGraph(user.FacebookId);
                        }

                        else
                            model.Photo = $"{BaseConfig.BaseUrl}{user.Photo}";

                        userRanking.Add(model);
                    }
                });

                var modelList = userRanking.OrderByDescending(x => x.Score).ToList();

                modelList.ForEach(user =>
                {
                    user.Position = position;
                    position++;
                });

                return Ok(Utilities.ReturnSuccess(data: modelList.OrderBy(x => x.Position)));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("MyPositionMonth")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult MyPositionMonth()
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var rangeStartDate = Utilities.ToTimeStamp(new DateTime(DateTime.UtcNow.Year, 11, 1, 0, 0, 0));
                var pointUsers = _pointUserService.FindBy(x => x.Created >= rangeStartDate && x._id != new ObjectId(userId)).ToList();
                var userRanking = new List<MiniUserViewModel>();
                var position = 1;
                
                pointUsers.ForEach(x =>
                {
                    if (!userRanking.Exists(user => user.Id.Equals(x.UserId)))
                    {
                        var user = _userService.FindById(x.UserId);
                        var model = _mapper.Map<MiniUserViewModel>(user);
                        model.Score = pointUsers.FindAll(u => u.UserId.Equals(x.UserId)).Sum(up => up.Points);

                        if (!string.IsNullOrEmpty(user.FacebookId))
                        {
                            model.Photo = Utilities.GetPhotoFacebookGraph(user.FacebookId);
                        }

                        else
                            model.Photo = $"{BaseConfig.BaseUrl}{user.Photo}";

                        userRanking.Add(model);
                    }
                });

                var modelList = userRanking.OrderByDescending(x => x.Score).ToList();

                modelList.ForEach(user =>
                {
                    user.Position = position;
                    position++;
                });
                return Ok(Utilities.ReturnSuccess(data: modelList.FirstOrDefault(x=> x.Id.Equals(userId))));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
