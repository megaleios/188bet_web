﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class AddressController : Controller
    {

        private readonly IMapper _mapper;
        private readonly IStateService _stateService;
        private readonly ICitieService _citieService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="stateService"></param>
        /// <param name="citieService"></param>
        public AddressController(IMapper mapper, IStateService stateService, ICitieService citieService)
        {
            _mapper = mapper;
            _stateService = stateService;
            _citieService = citieService;
        }

        /// <summary>
        /// Estados(UF)
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("States")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult States()
        {
            try
            {
                return Ok(Utilities.ReturnSuccess(data: _mapper.Map<List<StateViewModel>>(_stateService.FindAll().ToList())));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        /// CIDADES
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Cities/{stateId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Cities([FromRoute]string stateId)
        {
            try
            {
                if (string.IsNullOrEmpty(stateId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.StateIdEmpty));

                
                if (!_stateService.CheckBy(x => x._id.Equals(new ObjectId(stateId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.StateNotFound));
                
                return Ok(Utilities.ReturnSuccess(data: _mapper.Map<List<CitieViewModel>>(_citieService.FindBy(x=> x.StateId.Equals(stateId)))));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
