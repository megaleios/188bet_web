﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Repository.Interface;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class RewardsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IRewardService _rewardService;
        private readonly IDeliveryService _deliveryService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="userService"></param>
        /// <param name="rewardService"></param>
        public RewardsController(IMapper mapper, IUserService userService, IRewardService rewardService, IDeliveryService deliveryService)
        {
            _mapper = mapper;
            _userService = userService;
            _rewardService = rewardService;
            _deliveryService = deliveryService;
        }

        // GET: api/values
        /// <summary>
        /// List of Rewards
        /// </summary>
        /// <remarks>
        /// RECEIVED OBJ
        /// 
        ///         POST
        ///             {
        ///                 "minPrice": 0, // minimum value for filter
        ///                 "maxPrice": 0, // maximum value for filter
        ///                 "rewards": [ // list of Reswards
        ///                     {
        ///                         "name": "string",
        ///                         "description": "string",
        ///                         "price": 0,
        ///                         "photo": "string",
        ///                         "specifications": ["string"],
        ///                         "isRescued": false, // flag that indicates if user has already redeemed this reward
        ///                         "id": "string"
        ///                     }
        ///                 ]
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Get([FromQuery]int page, decimal? minPrice, decimal? maxPrice)
        {
            try
            {
                int limit = 10;
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var rewards = _rewardService.FindAll().OrderByDescending(x => x.Created);
                var filteredRewards = _mapper.Map<List<RewardViewModel>>(rewards);
                if (minPrice != null && maxPrice != null)
                    filteredRewards = filteredRewards.Where(x => x.Price >= minPrice && x.Price <= maxPrice).ToList();
                else if (minPrice != null)
                    filteredRewards = filteredRewards.Where(x => x.Price >= minPrice).ToList();
                else if (maxPrice != null)
                    filteredRewards = filteredRewards.Where(x => x.Price <= maxPrice).ToList();

                filteredRewards = filteredRewards.Skip(page * limit).Take(limit).ToList();

                filteredRewards.ForEach(reward => {
                    reward.IsRescued = false;
                    if (user.RewardsRescued != null && user.RewardsRescued.Contains(reward.Id))
                        reward.IsRescued = true;

                    if (String.IsNullOrEmpty(reward.Photo))
                        reward.Photo = $"{BaseConfig.BaseUrl}//imageNotFound.jpg";
                });

                ListRewardsViewModel model = new ListRewardsViewModel
                {
                    Rewards = filteredRewards,
                    MinPrice = rewards.Min(x => x.Price),
                    MaxPrice = rewards.Max(x => x.Price)
                };

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Details of Reward
        /// </summary>
        /// <remarks>
        /// RECEIVED OBJ
        /// 
        ///         POST
        ///             {
        ///                 "name": "string",
        ///                 "description": "string",
        ///                 "price": 0,
        ///                 "photo": "string",
        ///                 "specifications": ["string"],
        ///                 "isRescued": false, // flag that indicates if user has already redeemed this reward
        ///                 "id": "string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("Detail/{rewardId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Detail([FromRoute]string rewardId)
        { 
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (string.IsNullOrEmpty(rewardId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.RewardNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var reward = _rewardService.FindById(rewardId);

                var model = _mapper.Map<RewardViewModel>(reward);
                model.IsRescued = false;
                if (user.RewardsRescued != null && user.RewardsRescued.Contains(reward._id.ToString()))
                    model.IsRescued = true;

                if (String.IsNullOrEmpty(model.Photo))
                    model.Photo = $"{BaseConfig.BaseUrl}//imageNotFound.jpg";

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// Rescue a reward
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         POST
        ///             {
        ///                 "rewardId": "string", // required
        ///                 "name": "string", // required
        ///                 "cpf": "string", // required
        ///                 "cep": "string", // required
        ///                 "address": "string", // required
        ///                 "complement": "string", // not required, can be null
        ///                 "stateId": "string", // required
        ///                 "cityId": "string", // required
        ///                 "telephone": "string" // required
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("Rescue")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Rescue([FromBody]RescueRewardViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                var errors = ModelState.ValidModelState();

                if (errors != null)
                    return BadRequest(Utilities.ReturnErro(data: errors));

                if (!Utilities.ValidCpf(model.CPF))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.CPFInvalid));

                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var reward = _rewardService.FindById(model.RewardId);

                if (reward == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.RewardNotFound));

                // Checks if the user already has this reward
                if (user.RewardsRescued != null && user.RewardsRescued.Contains(reward._id.ToString()))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.AlreadyRescued));

                double price = Convert.ToDouble(reward.Price);

                // Checks if the user has enough cash
                if (user.Cashe < price)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.NoCashEnough));

                var delivery = _mapper.Map<Delivery>(model);
                delivery.UserId = user._id.ToString();
                _deliveryService.Create(delivery);

                user.Cashe -= price;
                if (user.RewardsRescued == null)
                    user.RewardsRescued = new List<string>();
                user.RewardsRescued.Add(reward._id.ToString());
                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess(data: null));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        private List<T> GetPage<T>(List<T> list, int page, int pageSize)
        {
            return list.Skip(page * pageSize).Take(pageSize).ToList();
        }
    }
}
