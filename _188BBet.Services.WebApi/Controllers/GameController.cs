﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Org.BouncyCastle.Asn1.Ocsp;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Data.Enum;

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class GameController : Controller
    {

        private readonly IMapper _mapper;
        private readonly IGameService _gameService;
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;
        private readonly IRoundService _roundService;
        private readonly IBetService _betService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="gameService"></param>
        /// <param name="userService"></param>
        /// <param name="teamService"></param>
        public GameController(IMapper mapper, IGameService gameService, IUserService userService, ITeamService teamService, IRoundService roundService, IBetService betService)
        {
            _mapper = mapper;
            _gameService = gameService;
            _userService = userService;
            _teamService = teamService;
            _roundService = roundService;
            _betService = betService;
        }
        
        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("Feed/{page}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Feed([FromRoute]int page)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);
                
                if(user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var games = _gameService.FindBy(x => x.Actived,page).ToList();
                var model = _mapper.Map<List<MiniGameViewModel>>(games.OrderBy(x => x.StartDate));

                model.ForEach(x =>
                {
                    var relatedCard = games.Where(c => c._id.ToString() == x.Id).FirstOrDefault();

                    // Teams
                    var teamOneId = relatedCard.TeamOne.FirstOrDefault().Value;
                    if (teamOneId != null) {
                        var teamOne = _mapper.Map<TeamViewModel>(_teamService.FindById(teamOneId));
                        if(String.IsNullOrEmpty(teamOne.Photo))
                            teamOne.Photo = $"{BaseConfig.BaseUrl}//imageNotFound.jpg";
                        x.TeamOne = teamOne;
                    }
                    
                    var teamTwoId = relatedCard.TeamTwo.FirstOrDefault().Value;
                    if (teamTwoId != null) {
                        var teamTwo = _mapper.Map<TeamViewModel>(_teamService.FindById(teamTwoId));
                        if (String.IsNullOrEmpty(teamTwo.Photo))
                            teamTwo.Photo = $"{BaseConfig.BaseUrl}//imageNotFound.jpg";
                        x.TeamTwo = teamTwo;
                    }

                    if (user.BetId_GameId.ContainsValue(x.Id))
                        x.Acquired = true;
                });

                //var model = new List<MiniGameViewModel>();

                //all.ForEach(x =>
                //{
                //    if(!user.BetId_GameId.ContainsValue(x.Id))
                //        model.Add(x);
                //});

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }
        
        /// <summary>
        ///  Cards Detail
        /// </summary>
        /// <remarks>
        /// SEND OBJ
        /// 
        ///         GET
        ///             {
        ///                 "title": "string",
        ///                 "startDate": 0,
        ///                 "endDate": 0,
        ///                 "actived": true,
        ///                 "type": 0, // GameEnum { 0 = National, 1 = Internaltional }
        ///                 "acquired": false,
        ///                 "cards": [{
        ///                     "title": "string",
        ///                     "question": "string",
        ///                     "type": 0, // TypeCard enum { 0 = TrueOrFalse, 1 = LeftAndRight, 2 = UpDown, 3 = ContainsTied }
        ///                     "options": [{
        ///                         "value": "string",
        ///                         "points": 0.0,
        ///                         "position": 0, // Position enum { 0 = Up, 1 = Down, 2 = Left, 3 = Right }
        ///                         "team": {
        ///                             "name": "string",
        ///                             "photo": "string",
        ///                             "id": "string"
        ///                         },
        ///                         "checked": false
        ///                     }]
        ///                 }],
        ///                 "id": "string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("Detail/{gameId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Detail([FromRoute]string gameId)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if(string.IsNullOrEmpty(gameId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GameNotFound));

                var user = _userService.FindById(userId);

                if(user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var game = _gameService.FindById(gameId);

                if (game == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GameNotFound));

                var model = _mapper.Map<GameDetailsViewModel>(game);

                // Check if is Acquired
                if (user.BetId_GameId.ContainsValue(model.Id))
                    model.Acquired = true;

                // Teams
                var teamOne = _mapper.Map<TeamViewModel>(_teamService.FindById(game.TeamOne.FirstOrDefault().Value));
                teamOne.SetPathInPhoto();

                var teamTwo = _mapper.Map<TeamViewModel>(_teamService.FindById(game.TeamTwo.FirstOrDefault().Value));
                teamTwo.SetPathInPhoto();

                // Bet
                var bet = _betService.FindBy(x => x.GameId == gameId && x.UserId == userId).FirstOrDefault();
                if (model.Acquired && bet != null) {
                    var cards = model.Cards.Where(x => x.Type == bet.Type).ToList();
                    cards.ForEach(card => {
                        if (card.Type == bet.Type) {
                            card.Checked = true;
                            card.Options.ForEach(option => {
                                if (option.Position == bet.UserChoice)
                                    option.Checked = true;
                            });
                        }
                    });
                    model.Cards = cards;
                }

                // Teams
                model.Cards.ForEach(card => {
                    card.Options.ForEach(option => {
                        var cardEntity = game.Cards.Where(x => x.Type == card.Type).FirstOrDefault();
                        if (cardEntity != null)
                        {
                            var optionEntity = cardEntity.Options.Where(x => x.Position == option.Position).FirstOrDefault();
                            if (optionEntity != null && optionEntity.TeamId != null){
                                var t = _mapper.Map<TeamViewModel>(_teamService.FindById(optionEntity.TeamId));
                                t.SetPathInPhoto();
                                option.Team = t;
                            }
                        }
                    });
                });

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }


        
        /// <summary>
        ///  Cards by rounds FAKE
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpGet("ByRound/{roundId}")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ByRound([FromRoute]string roundId)
        {
            try
            {
                var userId = Request.GetUserId();

                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                if (string.IsNullOrEmpty(roundId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.RoundNotFound));

                // TODO Fake Round ID
                var games = _gameService.FindAll(); //_gameService.FindBy(x => x.RoundId == roundId);
                var models = _mapper.Map<List<GameDetailsViewModel>>(games);

                models.ForEach(model =>
                {
                    var game = games.Where(x => x._id.ToString() == model.Id).FirstOrDefault();

                    // Check if is Acquired
                    if (user.BetId_GameId.ContainsValue(model.Id))
                        model.Acquired = true;

                    // Bet
                    var bet = _betService.FindBy(x => x.GameId == game._id.ToString() && x.UserId == userId).FirstOrDefault();
                    if (model.Acquired && bet != null)
                    {
                        var cards = model.Cards.Where(x => x.Type == bet.Type).ToList();
                        cards.ForEach(card => {
                            if (card.Type == bet.Type)
                            {
                                card.Checked = true;
                                card.Options.ForEach(option => {

                                    if (option.Position == bet.UserChoice)
                                        option.Checked = true;
                                });
                            }
                        });
                        model.Cards = cards;
                    }

                    // Teams
                    model.Cards.ForEach(card => {
                        card.Options.ForEach(option => {
                            var cardEntity = game.Cards.Where(x => x.Type == card.Type).FirstOrDefault();
                            if (cardEntity != null)
                            {
                                var optionEntity = cardEntity.Options.Where(x => x.Position == option.Position).FirstOrDefault();
                                if (optionEntity != null && optionEntity.TeamId != null){
                                    var t = _mapper.Map<TeamViewModel>(_teamService.FindById(optionEntity.TeamId));
                                    t.SetPathInPhoto();
                                    option.Team = t;
                                }
                            }
                        });
                    });
                });

                return Ok(Utilities.ReturnSuccess(data: models));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
