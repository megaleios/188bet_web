using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Newtonsoft.Json;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;
using _188Bet.Infra.Data.Entities;
using _188Bet.Application.Domain.MessagesDefault;
using AutoMapper;
using _188Bet.Infra.Data.Enum;
using UtilityFramework.Application.Core.JwtMiddleware;

namespace _188BBet.Services.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class TestController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ITeamService _teamService;
        private readonly IStateService _stateService;
        private readonly ICitieService _citieService;
        private readonly IGameService _gameService;
        private readonly IPointUserService _pointUserService;
        private readonly IBetService _betService;
        private readonly IUserService _userService;
        private readonly ITitleService _titleService;
        private readonly IRewardService _rewardService;

        public TestController(IMapper mapper, ITeamService teamService, IStateService stateService, ICitieService citieService, IGameService gameService, IPointUserService pointUserService, IBetService betService, IUserService userService, ITitleService titleService, IRewardService rewardService)
        {
            _mapper = mapper;
            _teamService = teamService;
            _stateService = stateService;
            _citieService = citieService;
            _gameService = gameService;
            _pointUserService = pointUserService;
            _betService = betService;
            _userService = userService;
            _titleService = titleService;
            _rewardService = rewardService;
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("ImportTeams")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ImportTeams()
        {
            try
            {

                var lines = System.IO.File
                    .ReadAllLines(
                        @"C:\Users\MegaMidia\source\repos\188 Bet\WebApi_188Bet\188Bet Docs\Teams.txt");

                foreach (var line in lines)
                {
                    _teamService.Create(new Team
                    {
                        Name = line
                    });
                }

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("ImportStates")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ImportStates()
        {
            try
            {
                var lines = System.IO.File
                    .ReadAllLines(
                        @"C:\Users\MegaMidia\source\repos\188 Bet\WebApi_188Bet\188Bet Docs\Estados.json");

                var objectJson = "";

                foreach (var line in lines)
                {
                    objectJson += line;
                }


                var result = JsonConvert.DeserializeObject<List<State>>(objectJson);

                result.ForEach(state =>
                {
                    _stateService.Create(state);
                });

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("ImportCities")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ImportCities()
        {
            try
            {
                var lines = System.IO.File
                    .ReadAllLines(
                        @"C:\Users\MegaMidia\source\repos\188 Bet\WebApi_188Bet\188Bet Docs\Cidades.json");

                var objectJson = "";

                foreach (var line in lines)
                {
                    objectJson += line;
                }

                var citieList = JsonConvert.DeserializeObject<List<Citie>>(objectJson);

                foreach (var citie in citieList)
                {
                    var state = _stateService.FindOneBy(x => x.Code.Equals(citie.Code));

                    if (state != null)
                    {
                        citie.StateId = state._id.ToString();
                        _citieService.Create(citie);
                    }
                }

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// CRIAR JOGO
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///         {
        ///             "title": "Times do Brasil ",
        ///             "teamOne": {
        ///                 "name": "ABC",
        ///                 "photo": null,
        ///                 "id": "5a1ef7ca5ab25c3d8c905216"
        ///             },
        ///             "teamTwo": {
        ///                 "name": "ASA",
        ///                 "photo": null,
        ///                 "id": "5a1ef7cb5ab25c3d8c905217"
        ///             },
        ///             "winner": null,
        ///             "startDate": 1512004208,
        ///             "endDate": 1512177008,
        ///             "teamOneWinPoints": 10,
        ///             "teamTwoWinPoints": 20,
        ///             "tiedPoints": 30,
        ///             "actived": true,
        ///             "type": 0
        ///         }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("NewGame")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult NewGame([FromBody]GameViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                var entity = _mapper.Map<Game>(model);

                var teamOne = _teamService.FindById(model.TeamOne.Id);
                var teamTwo = _teamService.FindById(model.TeamTwo.Id);

                entity.TeamOne.Add(teamOne._id.ToString(), teamOne.Name);
                entity.TeamTwo.Add(teamTwo._id.ToString(), teamTwo.Name);

                _gameService.Create(entity);

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///                 "teamOneWin": false,
        ///                 "teamTwoWin": false,
        ///                 "tied": true,
        ///                 "goalsTeamOne": 0,
        ///                 "goalsTeamTwo": 0,
        ///                 "id": "5a1f5b062c4c2541a876e52e"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("EndGame")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult EndGame([FromBody]EndGameViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.ModelNull));

                if (string.IsNullOrEmpty(model.Id))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GameNotFound));

                var gameId = ObjectId.Parse(model.Id);

                if (!_gameService.CheckBy(x => x._id.Equals(gameId)))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.GameNotFound));

                var game = _gameService.FindById(model.Id);

                if (model.TeamOneWin)
                    if (model.TeamTwoWin || model.Tied)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.OptionsBet));

                if (model.TeamTwoWin)
                    if (model.TeamOneWin || model.Tied)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.OptionsBet));

                if (model.Tied)
                    if (model.TeamOneWin || model.TeamOneWin)
                        return BadRequest(Utilities.ReturnErro(ValidMessages.OptionsBet));

                game.Actived = false;
                game.GoalsTeamOne = model.GoalsTeamOne;
                game.GoalsTeamTwo = model.GoalsTeamTwo;
                //game.TeamOneWin = model.TeamOneWin;
                //game.TeamTwoWin = model.TeamTwoWin;
                //game.Tied = model.Tied;
                _gameService.Update(game);


                var bets = _betService.FindBy(x => x.GameId.Equals(model.Id)).ToList();
                var betsSuccess = new List<Bet>();

                decimal points = 0;

                //if (game.TeamOneWin)
                //{
                //    points = game.TeamOneWinPoints;
                //    betsSuccess = bets.Where(x => x.TeamOneWin).ToList();
                //}

                //if (model.TeamTwoWin)
                //{
                //    points = game.TeamTwoWinPoints;
                //    betsSuccess = bets.Where(x => x.TeamTwoWin).ToList();
                //}

                //if (model.Tied)
                //{
                //    points = game.TiedPoints;
                //    betsSuccess = bets.Where(x => x.Tied).ToList();
                //}

                betsSuccess.ForEach(bet =>
                {
                    _pointUserService.Create(new PointUser
                    {
                        BetId = bet._id.ToString(),
                        GameId = game._id.ToString(),
                        UserId = bet.UserId,
                        Points = points
                    });
                    var user = _userService.FindById(bet.UserId);

                    if (user != null)
                    {
                        if (_gameService.CheckBy(x =>
                            x._id.Equals(new ObjectId(bet.GameId)) && x.Type.Equals(GameEnum.Internaltional)))
                            user.InternationalGameWinCount = user.InternationalGameWinCount++;

                        if (_gameService.CheckBy(x =>
                            x._id.Equals(new ObjectId(bet.GameId)) && x.Type.Equals(GameEnum.National)))
                            user.NationalGameWinCount = user.NationalGameWinCount++;

                        if (user.NationalGameWinCount == 2)
                            user.Medal = user.Medal + 2;

                        if (user.InternationalGameWinCount == 4)
                            user.Medal = user.Medal + 3;

                        user.Medal = user.Medal++;
                        _userService.Update(user);
                    }

                });

                bets.ForEach(bet =>
                {
                    if (!betsSuccess.Contains(bet))
                    {
                        _pointUserService.Create(new PointUser
                        {
                            BetId = bet._id.ToString(),
                            GameId = game._id.ToString(),
                            UserId = bet.UserId,
                            Points = 0
                        });
                    }
                });

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }


        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("AddNewTitle")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult AddNewTitles([FromBody]List<TitleViewModel> model)
        {
            try
            {

                model.ForEach(m =>
                {
                    _titleService.Create(new Title
                    {
                        Name = m.Name,
                        Description = m.Description,
                        Photo = m.Photo
                    });
                });

                return Ok(Utilities.ReturnSuccess(data: _titleService.FindAll()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("AddCardsToGames")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult AddCardsToGames()
        {
            try
            {
                var games = _gameService.FindAll().ToList();

                return Ok(Utilities.ReturnSuccess(data: null));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [HttpPost("AddNewReward")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult AddNewReward([FromBody] List<RewardViewModel> model)
        {
            try
            {
                model.ForEach(reward =>
                {
                    var entity = _mapper.Map<Reward>(reward);
                    if (entity != null)
                        _rewardService.Create(entity);
                });

                var rewards = _mapper.Map<List<RewardViewModel>>(_rewardService.FindAll());

                return Ok(Utilities.ReturnSuccess(data: rewards));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }


        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [Authorize("Bearer")]
        [HttpPost("AddCashToUser")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult AddCashToUser([FromQuery] double cash)
        {
            try
            {
                var userId = Request.GetUserId();

                // User validation
                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                user.Cashe += cash;

                _userService.Update(user);

                return Ok(Utilities.ReturnSuccess(data: null));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}