﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.Ocsp;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;
using AutoMapper;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    public class TeamController : Controller
    {

        private readonly IMapper _mapper;
        private readonly ITeamService _teamService;

        public TeamController(IMapper mapper, ITeamService teamService)
        {
            _mapper = mapper;
            _teamService = teamService;
        }

        /// <summary>
        /// Times do coração
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Get()
        {
            try
            {
                List<TeamViewModel> teams = _mapper.Map<List<TeamViewModel>>(_teamService.FindAll());
                teams.ForEach(t => {
                    if (String.IsNullOrEmpty(t.Photo)) 
                        t.Photo = $"{BaseConfig.BaseUrl}//imageNotFound.jpg";
                });
                return Ok(Utilities.ReturnSuccess(data: teams));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
