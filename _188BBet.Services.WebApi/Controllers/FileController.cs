﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Infra.Repository.Interface;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.JwtMiddleware;
using UtilityFramework.Application.Core.ViewModels;

namespace _188BBet.Services.WebApi.Controllers
{
    [Authorize("Bearer")]
    [Route("api/v1/[controller]")]
    /// <summary>
    /// 
    /// </summary>
    public class FileController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        public FileController(IHostingEnvironment env, IMapper mapper, IUserService userService)
        {
            _env = env;
            _mapper = mapper;
            _userService = userService;
        }
        
        /// <summary>
        /// Upload images
        /// </summary>
        /// <remarks>
        /// 
        ///     POST api/v1/profile/UpdatePassword
        ///                       
        ///         content-type: multipart/form-data
        ///         file = archive  
        ///         file = archive
        ///     
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        //[AllowAnonymous]
        [HttpPost("Upload")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Upload([FromForm] IFormFile file)
        {
            ;
            object response = null;
            try
            {
                #region DEPOIS DA POG

                var userId = Request.GetUserId();

                // User validation
                if (string.IsNullOrEmpty(userId))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.TokenInvalid));

                if (!_userService.CheckBy(x => x._id.Equals(new ObjectId(userId))))
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                var user = _userService.FindById(userId);

                if (user == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.UserNotFound));

                // File validation
                if (file == null)
                    return BadRequest(Utilities.ReturnErro(ValidMessages.FileNotFound));

                if (file.Length <= 0) return BadRequest(Utilities.ReturnErro(ValidMessages.FileNotFound));
                ;
                var folder = $"{_env.ContentRootPath}\\content\\upload\\";

                var exists = Directory.Exists(folder);

                if (!exists)
                    Directory.CreateDirectory(folder);

                var arquivo = Utilities
                    .GetUniqueFileName(Path.GetFileNameWithoutExtension(DateTime.Now.ToString("yy-MM-dd-HH-mm-ss")),
                        folder, Path.GetExtension(file.FileName)).ToLower();

                if (Path.GetExtension(file.FileName).Trim() == ".exe")
                    return BadRequest(Utilities.ReturnErro(ValidMessages.FileNotAllowed));

                arquivo = $"{arquivo}{Path.GetExtension(file.FileName)}";

                var pathFile = Path.Combine(folder, arquivo);

                using (var destinationStream = System.IO.File.Create(pathFile))
                {
                    await file.CopyToAsync(destinationStream);
                }

                response = new
                {
                    fileName = arquivo
                };

                #endregion

                #region ANTES DA POG

                //if (!files.Any()) return BadRequest(Utilities.ReturnErro("Nenhuma arquivo encontrado"));

                //foreach (var fileName in files)
                //{
                //    if (fileName.Length <= 0) continue;
                //    var folder = $"{_env.ContentRootPath}\\content\\upload\\";

                //    var exists = Directory.Exists(folder);

                //    if (!exists)
                //        Directory.CreateDirectory(folder);

                //    var arquivo = Utilities.GetUniqueFileName(Path.GetFileNameWithoutExtension(fileName.FileName), folder, Path.GetExtension(fileName.FileName)).ToLower();
                //    arquivo = $"{arquivo}{Path.GetExtension(fileName.FileName)}";

                //    var pathFile = Path.Combine(folder, arquivo);

                //    using (var destinationStream = System.IO.File.Create(pathFile))
                //    {
                //        await fileName.CopyToAsync(destinationStream);
                //    }

                //    response = new
                //    {
                //        fileName = arquivo
                //    };
                //}



                #endregion



                return Ok(Utilities.ReturnSuccess(data: response));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro(data: response));
            }
        }
    }
}
