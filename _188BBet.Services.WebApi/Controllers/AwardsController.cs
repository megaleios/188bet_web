﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Repository.Interface;

namespace _188BBet.Services.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    public class AwardsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="userService"></param>
        public AwardsController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }


        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Week")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Week()
        {
            try
            {
                var model = new List<AwardsWeek>
                {
                    new AwardsWeek
                    {
                        Name = "Uma camisa do seu time de futebol + um kit 188BET (Cooler + Mochila + Boné)",
                        Week = 1
                    },
                    new AwardsWeek
                    {
                        Name = "Um kit 188BET",
                        Week = 2
                    },
                    new AwardsWeek
                    {
                        Name = "Um boné 188BET",
                        Week = 3
                    }
                };

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Month")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult Month()
        {
            try
            {
                var model = new List<AwardsMonth>
                {
                    new AwardsMonth
                    {
                        Name = "IPHONE 7",
                        Month = 1
                    },
                    new AwardsMonth
                    {
                        Name = "TV LED 4K",
                        Month = 2
                    },
                    new AwardsMonth
                    {
                        Name = "Macbook Air",
                        Month = 3
                    },
                    new AwardsMonth
                    {
                        Name = "Uma viagem, com acompanhante para assistir Barcelona x Real Madrid em Miami",
                        Month = 4
                    },
                    new AwardsMonth
                    {
                        Name = "Uma viagem, com um acompanhante, para assistir a um jogo da Libertadores",
                        Month = 5
                    },
                    new AwardsMonth
                    {
                        Name = "Churrasqueira Elétrica",
                        Month = 6
                    },
                    new AwardsMonth
                    {
                        Name = "Assistir Brasil x Chile com direito a um acompanhante",
                        Month = 7
                    },
                    new AwardsMonth
                    {
                        Name = "Não definido.",
                        Month = 8
                    },
                    new AwardsMonth
                    {
                        Name = "Não definido.",
                        Month = 9
                    },
                    new AwardsMonth
                    {
                        Name = "Não definido.",
                        Month = 10
                    },
                    new AwardsMonth
                    {
                        Name = "Não definido.",
                        Month = 11
                    },
                    new AwardsMonth
                    {
                        Name = "Não definido.",
                        Month = 12
                    },
                };

                return Ok(Utilities.ReturnSuccess(data: model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
