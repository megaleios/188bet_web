﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class NotificationViewModel : BaseViewModel
    {
        public string Text { get; set; }
        public string Photo { get; set; }
        public bool IsRead { get; set; }
        public long Date { get; set; }
    }
}
