﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _188Bet.Application.Domain.ViewModels
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FacebookId { get; set; }
        public string RefreshToken { get; set; }
    }

    public class Device
    {
        public string DeviceId { get; set; }
    }
}
