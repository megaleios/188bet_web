﻿using System;
using System.Collections.Generic;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Data.Enum;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class CardViewModel
    {
        public CardViewModel()
        {
            Checked = false;
        }
        public string Title { get; set; }
        public string Question { get; set; }
        public TypeCard Type { get; set; }
        public bool Checked { get; set; }
        public List<OptionViewModel> Options { get; set; }
    }
}
