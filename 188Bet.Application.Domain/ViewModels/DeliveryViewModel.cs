﻿using System;
using System.ComponentModel.DataAnnotations;
using _188Bet.Application.Domain.MessagesDefault;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class DeliveryViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string CPF { get; set; }
        public string CEP { get; set; }
        public string Address { get; set; }
        public string Complement { get; set; }
        public CitieViewModel City { get; set; }
        public StateViewModel State { get; set; }
        public string Telephone { get; set; }
        public int Status { get; set; }
        public RewardViewModel Reward { get; set; }
        public UserViewModel User { get; set; }
    }

    public class RescueRewardViewModel
    {
        [Required(ErrorMessage = ValidMessages.RewardNotFound)]
        public string RewardId { get; set; }
        [Required(ErrorMessage = ValidMessages.FirstNameEmpty)]
        public string Name { get; set; }
        [Required(ErrorMessage = ValidMessages.CPFEmpty)]
        public string CPF { get; set; }
        [Required(ErrorMessage = ValidMessages.CEPEmpty)]
        public string CEP { get; set; }
        [Required(ErrorMessage = ValidMessages.AddressEmpty)]
        public string Address { get; set; }
        public string Complement { get; set; }
        [Required(ErrorMessage = ValidMessages.StateEmpty)]
        public string StateId { get; set; }
        [Required(ErrorMessage = ValidMessages.CitieEmpty)]
        public string CityId { get; set; }
        [Required(ErrorMessage = ValidMessages.TelephoneEmpty)]
        public string Telephone { get; set; }
    }
}
