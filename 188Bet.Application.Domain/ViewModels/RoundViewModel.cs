﻿using System;
using System.Collections.Generic;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class RoundViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public long BeginDate { get; set; }
        public long EndDate { get; set; }
        public int Points { get; set; }
        public int Position { get; set; }
    }
}
