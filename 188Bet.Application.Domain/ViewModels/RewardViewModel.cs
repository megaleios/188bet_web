﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using _188Bet.Application.Domain.MessagesDefault;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class RewardViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Photo { get; set; }
        public List<string> Specifications { get; set; }
        public bool IsRescued { get; set; }
    }

    public class ListRewardsViewModel
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public List<RewardViewModel> Rewards { get; set; }
    }
}
