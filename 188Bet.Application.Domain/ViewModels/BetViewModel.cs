﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Infra.Data.Enum;

namespace _188Bet.Application.Domain.ViewModels
{
    public class BetViewModel : BaseViewModel
    {
        public Position UserChoice { get; set; }
        public TypeCard Type { get; set; }
        public MiniGameViewModel Game { get; set; }
    }

    public class NewBetViewModel : BaseViewModel
    {
        public string UserId { get; set; }
        public Position UserChoice { get; set; }
        public TypeCard Type { get; set; }
        [Required(ErrorMessage = ValidMessages.GameIdEmpty)]
        public string GameId { get; set; }
    }
}
