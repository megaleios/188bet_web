﻿using System;
using System.ComponentModel.DataAnnotations;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Data.Enum;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class OptionViewModel
    {
        public string Value { get; set; }
        public double Points { get; set; }
        public Position Position { get; set; }
        [Required(ErrorMessage = ValidMessages.TeamNotFound)]
        public TeamViewModel Team { get; set; }
        public bool Checked { get; set; }
    }
}
