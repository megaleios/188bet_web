﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _188Bet.Application.Domain.ViewModels
{
    public class AwardViewModel
    {
        public string Name { get; set; }
    }

    public class AwardsWeek : AwardViewModel
    { 
        public int Week { get; set; }
    }

    public class AwardsMonth : AwardViewModel
    {
        public int Month { get; set; }
    }
}
