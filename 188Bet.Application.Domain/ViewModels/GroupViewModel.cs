﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UtilityFramework.Application.Core.ViewModels;
using UtilityFramework.Infra.Core.MongoDb.Data.Modelos;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Infra.Data.Enum;

namespace _188Bet.Application.Domain.ViewModels
{
    public class GroupViewModel : BaseViewModel
    {
        public GroupViewModel()
        {
            Members = new List<MemberGroupViewModel>();
        }
        [Required(ErrorMessage = ValidMessages.NameGroupEmpty)]
        public string Name { get; set; }
        public string Photo { get; set; }
        public List<MemberGroupViewModel> Members { get; set; }
        public List<InvitedUserGroupViewModel> InvitedUsers { get; set; }
    }

    public class MemberGroupViewModel
    {
        public string Id { get; set; }
        public string Photo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public bool IsAdmin { get; set; }
        public int Position { get; set; }
        public int Score { get; set; }
        public UserLevelEnum Level { get; set; }
    }

    public class InvitedUserGroupViewModel
    {
        public string Id { get; set; }
        public string Photo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
    }

    public class MiniGroupViewModel
    {
        public string Id { get; set; }
        public string Photo { get; set; }
        public string Name { get; set; }
        public int Members { get; set; }
        public int Position { get; set; }
    }
}
