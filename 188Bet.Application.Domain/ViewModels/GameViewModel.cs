﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Infra.Data.Enum;
using _188Bet.Infra.Data.Entities;

namespace _188Bet.Application.Domain.ViewModels
{
    public class GameViewModel : BaseViewModel
    {
        [Required(ErrorMessage = ValidMessages.TitleEmpty)]
        public string Title { get; set; }
        [Required(ErrorMessage = ValidMessages.TeamOneEmpty)]
        public TeamViewModel TeamOne { get; set; }
        [Required(ErrorMessage = ValidMessages.TeamTwoEmpty)]
        public TeamViewModel TeamTwo { get; set; }
        //public bool? Winner { get; set; }
        //[Required(ErrorMessage = ValidMessages.TeamOneWinPointEmpty)]
        //public decimal TeamOneWinPoints { get; set; }
        //[Required(ErrorMessage = ValidMessages.TeamTwoWinPointEmpty)]
        //public decimal TeamTwoWinPoints { get; set; }
        //[Required(ErrorMessage = ValidMessages.TiedPointEmpty)]
        //public decimal TiedPoints { get; set; }
        [Required(ErrorMessage = ValidMessages.StartDateEmptyGame)]
        public long StartDate { get; set; }
        [Required(ErrorMessage = ValidMessages.EndDateEmptyGame)]
        public long EndDate { get; set; }
        public int Bets { get; set; }
        public bool Actived { get; set; }
        public GameEnum Type { get; set; }
        public bool Acquired { get; set; }
        public int GoalsTeamOne { get; set; }
        public int GoalsTeamTwo { get; set; }
        public RoundViewModel Round { get; set; }
        public List<CardViewModel> Cards { get; set; }
    }

    public class MiniGameViewModel : BaseViewModel
    {
        public string Title { get; set; }
        public bool Acquired { get; set; }
        public TeamViewModel TeamOne { get; set; }
        public TeamViewModel TeamTwo { get; set; }
        public int Points { get; set; }
        public long EndDate { get; set; }
    }

    public class GameDetailsViewModel : BaseViewModel {
        public GameDetailsViewModel(){
            Points = 0;
        }

        public string Title { get; set; }
        public bool Acquired { get; set; }
        public long StartDate { get; set; }
        public long EndDate { get; set; }
        public GameEnum Type { get; set; }
        public int Points { get; set; }
        public List<CardViewModel> Cards { get; set; }
    }

    public class EndGameViewModel : BaseViewModel
    {
        public bool TeamOneWin { get; set; }
        public bool TeamTwoWin { get; set; }
        public bool Tied { get; set; }
        public int GoalsTeamOne { get; set; }
        public int GoalsTeamTwo { get; set; }
    }
}
