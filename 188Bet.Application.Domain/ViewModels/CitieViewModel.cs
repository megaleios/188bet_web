﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class CitieViewModel : BaseViewModel
    {
        public string Name { get; set; }
    }
}
