﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class TeamViewModel: BaseViewModel
    {
        public string Name { get; set; }
        public string Photo { get; set; }

        public void SetPathInPhoto()
        {
            if (!string.IsNullOrEmpty(Photo))
                Photo = $"{BaseConfig.BaseUrl}{Photo}";
            else
                Photo = $"{BaseConfig.DefaultUrl}";
        }
    }
}
