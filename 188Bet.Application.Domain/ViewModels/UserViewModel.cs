﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using RestSharp.Validation;
using Serilog.Events;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Application.Domain.MessagesDefault;
using _188Bet.Infra.Data.Enum;

namespace _188Bet.Application.Domain.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        public UserViewModel()
        {
            Titles = new List<string>();
        }
        public string Photo { get; set; }
        [Required(ErrorMessage = ValidMessages.FirstNameEmpty)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = ValidMessages.LastNameEmpty)]
        public string LastName { get; set; }
        [Required(ErrorMessage = ValidMessages.UsernameEmpty)]
        public string Username { get; set; }
        [Required(ErrorMessage = ValidMessages.EmailEmpty)]
        public string Email { get; set; }
        public string FacebookId { get; set; }
        [Required(ErrorMessage = ValidMessages.TeamEmpty)]
        public TeamViewModel Team { get; set; }
        [Required(ErrorMessage = ValidMessages.DateOfBirthEmpty)]
        public long DateOfBirth { get; set; }
        [Required(ErrorMessage = ValidMessages.StateEmpty)]
        public StateViewModel State { get; set; }
        [Required(ErrorMessage = ValidMessages.CitieEmpty)]
        public CitieViewModel Citie { get; set; }
        [Required(ErrorMessage = ValidMessages.PasswordEmpty)]
        [MinLength(6, ErrorMessage = ValidMessages.PasswordInvalid)]
        public string Password { get; set; }
        [Required(ErrorMessage = ValidMessages.ConfirmPasswordEmpty)]
        [Compare("Password", ErrorMessage = ValidMessages.ConfirmPasswordAndPasswordNotEquals)]
        public string ConfirmPassword { get; set; }
        public UserLevelEnum Level { get; set; }
        public int Medals { get; set; }
        public int Position { get; set; }
        public List<string> Titles { get; set; }
        public void NewUser()
        {
            Level = UserLevelEnum.New;
            Medals = 0;
            Photo = "ProfileDefault.jpg";
        }

        public void SetPathInPhoto()
        {
            if (!string.IsNullOrEmpty(FacebookId))
                Photo = Utilities.GetPhotoFacebookGraph(FacebookId);
            else if (!string.IsNullOrEmpty(Photo))
                Photo = $"{BaseConfig.BaseUrl}{Photo}";
            else
                Photo = $"{BaseConfig.BaseUrl}ProfileDefault.jpg";
        }
    }

    public class MiniUserViewModel : BaseViewModel
    {
        public string Photo { get; set; }
        public UserLevelEnum Level { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string FacebookId { get; set; }
        public int Medals { get; set; }
        public double Cashe { get; set; }
        public int Position { get; set; }
        public decimal Score { get; set; }

        public void SetPathInPhoto()
        {
            if (!string.IsNullOrEmpty(FacebookId))
                Photo = Utilities.GetPhotoFacebookGraph(FacebookId);
            else if (!string.IsNullOrEmpty(Photo))
                Photo = $"{BaseConfig.BaseUrl}{Photo}";
            else
                Photo = $"{BaseConfig.BaseUrl}ProfileDefault.jpg";
        }
    }

    public class UserUpdateViewModel : BaseViewModel
    {
        public string Photo { get; set; }
        [Required(ErrorMessage = ValidMessages.FirstNameEmpty)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = ValidMessages.LastNameEmpty)]
        public string LastName { get; set; }
        [Required(ErrorMessage = ValidMessages.EmailEmpty)]
        public string Email { get; set; }
        [Required(ErrorMessage = ValidMessages.TeamEmpty)]
        public TeamViewModel Team { get; set; }
        [Required(ErrorMessage = ValidMessages.StateEmpty)]
        public StateViewModel State { get; set; }
        [Required(ErrorMessage = ValidMessages.CitieEmpty)]
        public CitieViewModel Citie { get; set; }
    }

    public class PasswordViewModel
    {
        public string OldPassword { get; set; }
        [Required(ErrorMessage = ValidMessages.PasswordEmpty)]
        [MinLength(6, ErrorMessage = ValidMessages.PasswordInvalid)]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = ValidMessages.ConfirmPasswordEmpty)]
        [Compare("NewPassword", ErrorMessage = ValidMessages.ConfirmPasswordAndPasswordNotEquals)]
        public string ConfirmNewPassword { get; set; }
    }
}
