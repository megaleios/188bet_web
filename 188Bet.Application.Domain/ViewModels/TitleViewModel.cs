﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Application.Core.ViewModels;

namespace _188Bet.Application.Domain.ViewModels
{
    public class TitleViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public bool Acquired { get; set; }
    }
}
