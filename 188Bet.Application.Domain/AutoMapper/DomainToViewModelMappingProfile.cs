﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Data.Entities;

namespace _188Bet.Application.Domain.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            //firebase
            //CreateMap<Chat, ChatViewModel>();
            //CreateMap<Members, MembersViewModel>();
            //CreateMap<Messages, DescriptionViewModel>();
            //CreateMap<Schedule, MyScheduleViewModel>();

            CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.ConfirmPassword, opt => opt.Ignore())
                .ForMember(dest => dest.State, opt => opt.Ignore())
                .ForMember(dest => dest.Citie, opt => opt.Ignore())
                .ForMember(dest => dest.Team, opt => opt.Ignore());

            CreateMap<User, MiniUserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id));
            
            CreateMap<Team, TeamViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src=> src._id));

            CreateMap<Citie, CitieViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id));

            CreateMap<State, StateViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id));

            CreateMap<Round, RoundViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id));

            CreateMap<Game, MiniGameViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.TeamOne, opt => opt.Ignore())
                .ForMember(dest => dest.TeamTwo, opt => opt.Ignore());

            CreateMap<Game, GameDetailsViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.Points, opt => opt.Ignore());

            CreateMap<Game, GameViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.TeamOne, opt => opt.Ignore())
                .ForMember(dest => dest.TeamTwo, opt => opt.Ignore())
                .ForMember(dest => dest.Round, opt => opt.Ignore());

            CreateMap<Card, CardViewModel>();

            CreateMap<Option, OptionViewModel>()
                .ForMember(dest => dest.Team, opt => opt.Ignore());

            CreateMap<Group, GroupViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id));

            CreateMap<Reward, RewardViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.IsRescued, opt => opt.Ignore());

            CreateMap<Delivery, DeliveryViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.State, opt => opt.Ignore())
                .ForMember(dest => dest.City, opt => opt.Ignore())
                .ForMember(dest => dest.Reward, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore());

            CreateMap<User, MemberGroupViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.Score, opt => opt.MapFrom(src => src.Score))
                .ForMember(dest => dest.Level, opt => opt.MapFrom(src => src.Level))
                .ForMember(dest => dest.Photo, opt => opt.Ignore());

            CreateMap<User, InvitedUserGroupViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src._id));
        }
    }
}
