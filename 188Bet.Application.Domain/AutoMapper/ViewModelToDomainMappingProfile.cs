﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MongoDB.Bson;
using _188Bet.Application.Domain.ViewModels;
using _188Bet.Infra.Data.Entities;

namespace _188Bet.Application.Domain.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            //firebase
            //CreateMap<ChatViewModel, Chat>();
            //CreateMap<MembersViewModel, Members>();
            //CreateMap<DescriptionViewModel, Messages>();

            CreateMap<UserViewModel, User>();
                //.ForMember(dest => dest.Team, opt => opt.MapFrom(src => new Dictionary<string, string> { { src.Team.Name, src.Team.Id } }))
                //.ForMember(dest => dest.Citie, opt => opt.MapFrom(src => new Dictionary<string, string> { { src.Citie.Name, src.Citie.Id } }))
                //.ForMember(dest => dest.State, opt => opt.MapFrom(src => new Dictionary<string, string> { { src.State.Name, src.State.Id } }));
            CreateMap<GameViewModel, Game>();
            CreateMap<NewBetViewModel, Bet>();
            CreateMap<GroupViewModel, Group>();
            CreateMap<CardViewModel, Card>();
            CreateMap<OptionViewModel, Option>()
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.Team.Id));
            CreateMap<RewardViewModel, Reward>();
            CreateMap<RescueRewardViewModel, Delivery>();
        }
    }
}
