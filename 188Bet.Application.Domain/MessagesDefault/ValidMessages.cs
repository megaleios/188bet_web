﻿namespace _188Bet.Application.Domain.MessagesDefault
{
    public class ValidMessages
    {
        //Authorize Message
        public const string TokenInvalid = "Sessão expirou!";
        public const string AgeInvalid = "Só é permitido usuarios maiores de 18 anos!";

        //Date Exist Messages
        public const string UsernameExist = "Nome de usuario já esta em uso!";
        public const string EmailExist = "Email já esta em uso!";

        //Invalid Messages
        public const string UsernameInvalid = "Nome de usuario invalido!";
        public const string EmailInvalid = "Email invalido!";
        public const string PasswordInvalid = "Senha invalida, sua senha deve conter pelo menos 6 characters!";
        public const string ConfirmPasswordInvalid = "Senhas não conhecidem!";
        public const string PasswordInvalidLogin = "Senha incorreta!";
        public const string CPFInvalid = "CPF invalido!";

        //Value Empty Messages
        public const string StateIdEmpty = "Informe o Estado(UF)!";
        public const string FirstNameEmpty = "Nome obrigatorio!";
        public const string LastNameEmpty = "Sobrenome obrigatorio!";
        public const string EmailEmpty = "Email obrigatorio!";
        public const string UsernameEmpty = "Nome de usuario obrigatorio!";
        public const string TeamEmpty = "Informe o time do coração!";
        public const string DateOfBirthEmpty = "Informe a data de nascimento!";
        public const string StateEmpty = "Estado(UF) obrigatorio!";
        public const string CitieEmpty = "Cidade obrigatoria!";
        public const string PasswordEmpty = "Senha Obrigatorioa!";
        public const string ConfirmPasswordEmpty = "Senha Obrigatorioa!";
        public const string ConfirmPasswordAndPasswordNotEquals = "Senhas não são identicas!";
        public const string DeviceIdEmpty = "DeviceId invalido!";
        public const string CPFEmpty = "CPF obrigatorio!";
        public const string CEPEmpty = "CEP obrigatorio!";
        public const string AddressEmpty = "Endereço obrigatorio!";
        public const string TelephoneEmpty = "Telefone obrigatorio!";

        //Not Found Messages
        public const string StateNotFound = "Estado(UF) não encontrado!";
        public const string CitieNotFound = "Cidade não encontrada!";
        public const string TeamNotFound = "Time não encontrado!";
        public const string UserNotFound = "Usuario não encontrado!";
        public const string GameNotFound = "Jogo não encontrado!";
        public const string RewardNotFound = "Recompensa não encontrada!";
        public const string FileNotFound = "Nenhum arquivo encontrado!";
        public const string BetNotFound = "Nenhum palpite encontrado!";
        public const string RoundNotFound = "Nenhuma rodada encontrado!";

        //Value Null Messages
        public const string ModelNull = "Informe os valores obrigatorios!";

        //File Messages
        public const string FileNotAllowed = "Arquivo não permitido!";

        //Game Messages
        public const string TitleEmpty = "Informe o titulo do jogo!";
        public const string TeamOneEmpty = "Informe o primeiro time!";
        public const string TeamTwoEmpty = "Informe o segundo time!";
        public const string StartDateEmptyGame = "Informe a data de inicio do jogo!";
        public const string EndDateEmptyGame = "Informe a data de inicio do jogo!";
        public const string TeamOneWinPointEmpty = "Informe quantos pontos vale caso o time 1 Ganhe!";
        public const string TeamTwoWinPointEmpty = "Informe quantos pontos vale caso o time 2 Ganhe!";
        public const string TiedPointEmpty = "Informe quantos pontos vale caso o jogo acabe em empate!";

        //Bet Messages
        public const string GameIdEmpty = "Informe o jogo!";
        public const string OptionsBet = "Selecione apenas uma opção, Time um vencedor/Time dois vencedor/Empate!";
        public const string SelectOptionBet = "Selecione uma das opções!";
        public const string BetExist = "É Permitido apenas um chute por jogo!";

        //Group Messages
        public const string NameGroupEmpty = "Informe o nome do grupo!";
        public const string QuantityMembersInvalid = "Não é possivel criar um grupo apenas com um usuario!";
        public const string UniqueAdminMember = "Só é permitido um admin por grupo";
        public const string UserNotFoundGroup = "Não disponivel, ou não encontrado!";
        public const string GroupNameExist = "Nome de grupo ja utilizado!";
        public const string NoContainsRequestForGroup = "Convite não encontrado, cerifique-se de ter recebido um convite para participar deste grupo!";
        public const string GroupNotFound = "Grupo não encontrado!";
        public const string ParticipatesGroup = "Você ja esta neste grupo!";
        public const string NotParticipatesGroup = "Você não faz parte deste grupo!";
        public const string GroupIdEmpty = "Informe o id do grupo que deseja entrar!";
        public const string NoSearchNewAdmin = "Não foi possivel encontrar um novo administrado ao grupo!";
        public const string YouNoAdminGroup = "Apenas o administrador do grupo tem acesso a essa função!";
        public const string TargetUserNotFound = "Usuario selecionado não encontrado!";
        public const string TargetUserNotParticipateGroup = "Usuario selecionado não faz parte deste grupo!";

        //Rewards Messages
        public const string NoCashEnough = "Usuário não tem dinheiro suficiente!";
        public const string AlreadyRescued = "Usuário já resgatou essa recompensa!";

        //Messages Success
        public const string EnterInGroup = "Parabéns você acaba de entrar no grupo: ";
        public const string DennyRequestGroup = "Solicitação de grupo excluida!";
        public const string ExitGroupSuccess = "Você saiu do grupo: ";
        public const string GroupDeleted = "Você deletou o grupo: ";
        public const string DeleteOneItem = "É permitido deletar apenas um item por vez!";
        public const string SelectOneItemForDelete = "Selecione um item para deletar!";
        public const string SelectOneItemForAdd = "Selecione um item para adicionar!";
        public const string PasswordUpdatedSuccess = "Senha atualizada com sucesso!";

        //SYSTEM
        public const string SystemName = "188Bet";
    }
}
