﻿using System;
using _188Bet.Infra.Data.Entities;
using UtilityFramework.Infra.Core.MongoDb.Business;

namespace _188Bet.Infra.Repository.Interface
{
    public interface IRewardService : IBusinessBase<Reward>
    {
    }
}
