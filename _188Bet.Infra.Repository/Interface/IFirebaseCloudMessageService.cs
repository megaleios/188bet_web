﻿using System;
using System.Collections.Generic;
using System.Text;
using FCM.Net;

namespace _188Bet.Infra.Repository.Interface
{
    public interface IFirebaseCloudMessageService
    {
        string SendPushNotification(string title, string msg, List<string> deviceList, Priority priority,DateTime dateNotification, string serverKey);
    }
}
