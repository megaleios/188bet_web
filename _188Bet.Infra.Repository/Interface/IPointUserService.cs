﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityFramework.Infra.Core.MongoDb.Business;
using UtilityFramework.Infra.Repository.Core.Interface;
using _188Bet.Infra.Data.Entities;

namespace _188Bet.Infra.Repository.Interface
{
    public interface IPointUserService : IBusinessBase<PointUser>
    {
    }
}
