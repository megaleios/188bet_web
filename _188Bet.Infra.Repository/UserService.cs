﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using UtilityFramework.Infra.Core.MongoDb.Business;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Repository.Interface;
using System.Text.RegularExpressions;

namespace _188Bet.Infra.Repository
{
    public class UserService : BusinessBase<User>, IUserService
    {
        public User GetByUsername(string username)
        {
            var pattern = new Regex("^" + username + "$", RegexOptions.IgnoreCase);
            _query = Query<User>.EQ(x => x.Username, new BsonRegularExpression(pattern));
            return GetCollection().FindOne(_query);
        }
    }
}
