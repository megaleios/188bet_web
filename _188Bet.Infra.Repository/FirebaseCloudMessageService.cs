﻿using System;
using System.Collections.Generic;
using System.Text;
using FCM.Net;
using _188Bet.Infra.Repository.Interface;

namespace _188Bet.Infra.Repository
{
    public class FirebaseCloudMessageService : IFirebaseCloudMessageService
    {
        public string SendPushNotification(string title, string msg, List<string> deviceList, Priority priority, DateTime dateNotification, string serverKey)
        {
            using (var sender = new Sender(serverKey))
            {
                var message = new Message
                {
                    RegistrationIds = deviceList,
                    Notification = new Notification
                    {
                        Title = title,
                        Body = msg
                    },
                    Priority = priority
                };
                var result = sender.SendAsync(message).Result;

                if (result.MessageResponse.Results != null)
                    return result.MessageResponse.MulticastId;

                return "";
            }
        }
    }
}
