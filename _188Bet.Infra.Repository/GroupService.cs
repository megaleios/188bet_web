﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using UtilityFramework.Infra.Core.MongoDb.Business;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Repository.Interface;

namespace _188Bet.Infra.Repository
{
    public class GroupService : BusinessBase<Group>, IGroupService
    {
        public bool CheckBySensitiveKey(string name)
        {
            _query = Query<Group>.Matches(x => x.Name, new BsonRegularExpression(name, "i"));
            return _query == null;
        }
    }
}
