﻿using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UtilityFramework.Application.Core;
using UtilityFramework.Application.Core.ViewModels;
using _188Bet.Infra.Data.Entities;
using _188Bet.Infra.Repository.Interface;

namespace _188Bet.Services.WebAPI.Controllers
{
    [Route("api/v1/Imports")]
    public class ImportController : Controller
    {
        private readonly ITeamService _teamService;

        public ImportController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        /// <summary>
        /// DESCRIPTION METHOD
        /// </summary>
        /// <remarks>
        /// OBJ DE ENVIO
        /// 
        ///         POST
        ///             {
        ///              "":"string"
        ///             }
        /// </remarks>
        /// <response code="200">Returns success</response>
        /// <response code="400">Custom Error</response>
        /// <response code="401">Unauthorize Error</response>
        /// <response code="500">Exception Error</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("ImportTeams")]
        [ProducesResponseType(typeof(ReturnViewModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public IActionResult ImportTeams([FromBody] object model)
        {
            try
            {

                var lines = System.IO.File
                    .ReadAllLines(
                        @"C:\Users\MegaMidia\source\repos\188 Bet\WebApi_188Bet\188Bet Docs\Teams.txt");

                foreach (var line in lines)
                {
                    _teamService.Create(new Team
                    {
                        Name = line
                    });
                }

                return Ok(Utilities.ReturnSuccess());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ReturnErro());
            }
        }

    }
}
